import pyAMI.client
import pyAMI.atlas.api as AtlasAPI
import os, sys

client = pyAMI.client.Client('atlas')
AtlasAPI.init()

######################################################################################################
# EDIT
######################################################################################################

samples = [ # Example
  "mc15_14TeV.147911.Pythia8_AU2CT10_jetjet_JZ1W.recon.AOD.e2403_s3142_s3143_r9589",
  "mc15_14TeV.147912.Pythia8_AU2CT10_jetjet_JZ2W.recon.AOD.e2403_s3142_s3143_r9589",
  "mc15_14TeV.147913.Pythia8_AU2CT10_jetjet_JZ3W.recon.AOD.e2403_s3142_s3143_r9589",
  "mc15_14TeV.147914.Pythia8_AU2CT10_jetjet_JZ4W.recon.AOD.e2403_s3142_s3143_r9589",
  "mc15_14TeV.147915.Pythia8_AU2CT10_jetjet_JZ5W.recon.AOD.e2403_s3142_s3143_r9589",
  "mc15_14TeV.147916.Pythia8_AU2CT10_jetjet_JZ6W.recon.AOD.e2403_s3142_s3143_r9589",
  "mc15_14TeV.147917.Pythia8_AU2CT10_jetjet_JZ7W.recon.AOD.e2403_s3142_s3143_r9589",
]

###########################################################################################################
# DO NOT MODIFY
###########################################################################################################

# Loop over samples
print "DSID \t xs \t eff \n" 
for dataset in samples:
  run = dataset.split(".",2)
  info = str(run[1])
  prov = AtlasAPI.get_dataset_prov(client,dataset)
  arr = prov['node']
  for dict in arr:
    d = dict['logicalDatasetName']
    if run[1] in d and "minbias" not in d:
      if "evgen" in d:
        dd = AtlasAPI.get_dataset_info(client,d)
        xsec = dd[0]['crossSection']
        eff = dd[0]['genFiltEff']
        break
  info += " " + str(xsec) + " " + str(eff) 
  print info			  
	
