/* **********************************************************************\
 *                                                                      *
 *  #   Name:   TreetoHists      	                                *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 24/08/18 First version              J. Bossio (jbossios@cern.ch)  *
\************************************************************************/

#include "../TreetoHists/TreetoHists.h"

// Variables used to estimate closure
bool applyInsitu = false;
TString insituFile = "";
TString rel_histoname = "JETALGO_EtaInterCalibration";
TString PATH_user = "";
TString InputFile_user = "";

// Selection Cuts
float ptMin     = 20;
float detEtaMax = 3.0;

// MC Campaign
bool MC15c = false;
bool HLLHC_optimistic   = false;
bool HLLHC_conservative = false;
bool ComputeSampleWeight = true;
//bool Bootstrap = false;
bool m_debug      = false;

void EnableBranches();
void SetBranchAddress();
void BookHistograms();

struct SortByPt {
  template <class A, class B> bool operator() (A a, B b){
    return a.Pt() >= b.Pt();
  }
};

bool myfunction (int i,int j);

int main(int argc, char* argv[]) {

  gROOT->ProcessLine(".L loader.C+");
  
  std::string systName = "";
  std::string pathUser = "";
  std::string inputFileUser = "";
  //---------------------------
  // Decoding the user settings
  //---------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
        v.push_back(item);
    }

    if ( opt.find("--pathUSER=")   != std::string::npos) pathUser = v[1];
    if ( opt.find("--inputFileUSER=")   != std::string::npos) inputFileUser = v[1];
    if ( opt.find("--syst=")   != std::string::npos) systName = v[1];
    
    if ( opt.find("--MC15c=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) MC15c = true;
      if (v[1].find("FALSE") != std::string::npos) MC15c = false;
    }

    if ( opt.find("--HLLHCOptimistic=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) HLLHC_optimistic = true;
      if (v[1].find("FALSE") != std::string::npos) HLLHC_optimistic = false;
    }

    if ( opt.find("--HLLHCConservative=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) HLLHC_conservative = true;
      if (v[1].find("FALSE") != std::string::npos) HLLHC_conservative = false;
    }

    if ( opt.find("--debug=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_debug= true;
      if (v[1].find("FALSE") != std::string::npos) m_debug= false;
    }

    /*if ( opt.find("--Bootstrap=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) Bootstrap= true;
      if (v[1].find("FALSE") != std::string::npos) Bootstrap= false;
    }*/

    if ( opt.find("--computeSampleWeights=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) ComputeSampleWeight= true;
      if (v[1].find("FALSE") != std::string::npos) ComputeSampleWeight= false;
    }
    
  }//End: Loop over input options

  PATH_user = pathUser;
  InputFile_user = inputFileUser;

  // Protections
  if(HLLHC_conservative && HLLHC_optimistic){
    std::cout << "ERROR: Choose HLLHC conservative, HLLHC optimistic or MC15c, exiting" << std::endl;
    return 1;
  }
  if(HLLHC_conservative && MC15c){
    std::cout << "ERROR: Choose HLLHC conservative, HLLHC optimistic or MC15c, exiting" << std::endl;
    return 1;
  }
  if(HLLHC_optimistic && MC15c){
    std::cout << "ERROR: Choose HLLHC conservative, HLLHC optimistic or MC15c, exiting" << std::endl;
    return 1;
  }
  if(!HLLHC_conservative && !HLLHC_optimistic && !MC15c){
    std::cout << "ERROR: Choose HLLHC conservative, HLLHC optimistic or MC15c, exiting" << std::endl;
    return 1;
  }
  if(PATH_user=="")      std::cout << "ERROR: No pathUSER specified in Run.py" << std::endl;
  if(InputFile_user=="") std::cout << "ERROR: No inputFileUSER specified in Run.py" << std::endl;

  if(systName=="nominal" || m_debug){
    if(MC15c) std::cout<< "Running MC15c"<< std::endl;
    else if(HLLHC_conservative) std::cout << "Running HLLHC conservative" << std::endl;
    else std::cout<< "Running HLLHC optimistic"<< std::endl;
    std::cout << "SystName: Nominal" << std::endl;
  }
  
  // OutputFile
  TString outputFile = "Results/";
  if(MC15c) outputFile += "MC15c/";
  else if(HLLHC_conservative) outputFile += "HLLHC_conservative/";
  else outputFile += "HLLHC_optimistic/";
  if(systName!="nominal") outputFile += "JET_";
  outputFile += systName;
  outputFile += "_";
  outputFile += InputFile_user;

  //-----------------------------
  // Defining eta bins
  //-----------------------------
 
  for (unsigned int i = 0; i <= netaBins; i++ ) {
    etaBins[i]= -4.5 + i/10.0;
  }
 
  //-----------------------
  // Getting File
  //-----------------------

  TString inputFile = PATH_user;
  inputFile += InputFile_user;
  std::cout << "Opening: " << inputFile << std::endl;
  ft = TFile::Open(inputFile.Data());
  ft->cd();
  if(ft == NULL) {
    std::cout<<"Returned null file: "<<ft<<", exiting"<<std::endl;
    return 1;
  }
  
  //-----------------------
  // Getting Tree
  //-----------------------

  TString treeName = "";
  if(systName=="nominal") treeName = systName;
  else treeName += "JET_"+systName;
  TString directoryName = "TreeAlgo";
  TDirectory* TDirF_tmp = ft->GetDirectory(directoryName.Data());
  TDirF_tmp->GetObject(treeName.Data(), tree);
   
  //---------------------------
  // Enable necessary branches
  //---------------------------
  EnableBranches();
  SetBranchAddress();
  // Number of events
  int entries = tree->GetEntries();
  if(m_debug) std::cout << "Total number of entries: " << entries << std::endl;
  if(m_debug) entries = 10;
  if(m_debug) std::cout << "Running over " << entries << " entries" << std::endl;

  //---------------------
  // Booking Histograms
  //---------------------
  if(m_debug) std::cout << "Booking Histograms" << std::endl;
  BookHistograms();
  if(m_debug) std::cout << "Histograms Booked" << std::endl;

  double weight = 1.;
  
  int Skippedevents_Cleaning = 0;
  
  // Get pyAMI values and metadata when computing sample weights
  double xs  = 1;
  double eff = 1;
  double Denominator = 1;
  if(ComputeSampleWeight){

    if(PATH_user.Contains("JZ1")) JZSlice = "1";
    if(PATH_user.Contains("JZ2")) JZSlice = "2";
    if(PATH_user.Contains("JZ3")) JZSlice = "3";
    if(PATH_user.Contains("JZ4")) JZSlice = "4";
    if(PATH_user.Contains("JZ5")) JZSlice = "5";
    if(PATH_user.Contains("JZ6")) JZSlice = "6";
    if(PATH_user.Contains("JZ7")) JZSlice = "7";

    TH1D* cutflow; // Histogram with the total number of events
    TString cutflowhistoname;
    cutflowhistoname = "MetaData_EventCount";
    if(systName=="nominal") std::cout << "Getting TH1D: " << cutflowhistoname << std::endl;
    // Open File metadata file
    TString TotalNevents1file = PATH_user;
    //TotalNevents1file += "JZ"+JZSlice+"_metadata/JZ"+JZSlice+"_metadata.root";
    TotalNevents1file += "JZ"+JZSlice+"_metadata.root";
    TFile* file1events = new TFile(TotalNevents1file,"read");
    // Getting the number of events for this JZX sample
    cutflow = (TH1D*)file1events->Get(cutflowhistoname);
    Denominator = cutflow->GetBinContent(1);
    if(systName=="nominal") std::cout << "Number of events: " << Denominator << std::endl;
    // Open File with AMI values
    std::vector<double> runNumbers;
    std::vector<double> xss;
    std::vector<double> effs;
    // Input File
    std::ifstream in;
    TString AMIfile = "../pyAMI/Outputs/";
    if(MC15c) AMIfile += "MC15c";
    else AMIfile += "HLLHC";
    AMIfile += ".txt";
    if(systName=="nominal") std::cout << "Reading AMI values from: " << AMIfile << std::endl;
    in.open(AMIfile,std::ifstream::in);
    double t0, t1, t2;
    // Reading values
    while (in >> t0 >> t1 >> t2) {
      std::vector<double> temp;
      runNumbers.push_back(t0);
      xss.push_back(t1);
      effs.push_back(t2);
    }
    in.close();
    bool runFound = false;
    int RunNumber = 1;
    if(MC15c){
      if(PATH_user.Contains("JZ1")) RunNumber = 361021;
      if(PATH_user.Contains("JZ2")) RunNumber = 361022;
      if(PATH_user.Contains("JZ3")) RunNumber = 361023;
      if(PATH_user.Contains("JZ4")) RunNumber = 361024;
      if(PATH_user.Contains("JZ5")) RunNumber = 361025;
      if(PATH_user.Contains("JZ6")) RunNumber = 361026;
      if(PATH_user.Contains("JZ7")) RunNumber = 361027;
    } else {
      if(PATH_user.Contains("JZ1")) RunNumber = 147911;
      if(PATH_user.Contains("JZ2")) RunNumber = 147912;
      if(PATH_user.Contains("JZ3")) RunNumber = 147913;
      if(PATH_user.Contains("JZ4")) RunNumber = 147914;
      if(PATH_user.Contains("JZ5")) RunNumber = 147915;
      if(PATH_user.Contains("JZ6")) RunNumber = 147916;
      if(PATH_user.Contains("JZ7")) RunNumber = 147917;
    }
    for(unsigned int irun=0;irun<runNumbers.size();++irun){
      if(RunNumber==runNumbers.at(irun)){
        runFound = true;
        xs  = xss.at(irun);
        eff = effs.at(irun);
      }
    }
    if(!runFound){
      std::cout << "RunNumber not found in AMI file, exiting" << std::endl;
      return 1;
    }
  } // end ComputeSampleWeight

  //------------------
  // Loop over entries
  //------------------
  if(systName=="nominal") std::cout << "Loop over entries" << std::endl;
  for (int entry=0; entry<entries; ++entry) {
    
    //---------------
    // Cleaning values
    //---------------
    
    if(m_debug) std::cout << "Cleaning vectors" << std::endl;

    runNumber = -999;
    eventNumber = -999;
    mcEventNumber = -999;
    mcEventWeight = -999;

    // Cleaning vectors
    Jet_pt->clear();
    Jet_E->clear();
    Jet_phi->clear();
    Jet_eta->clear();
    Jet_clean_passLooseBad->clear();
    TruthJet_pt->clear();
    TruthJet_E->clear();
    TruthJet_phi->clear();
    TruthJet_eta->clear();

    // Get entry
    bool should_skip = false; // Jet Cleaning
    if(m_debug) std::cout << "Event number: " << entry << std::endl;
    if(m_debug) std::cout << "Before GetEntry()" << std::endl;
    tree->GetEntry(entry);
    if(m_debug) std::cout << "After GetEntry()" << std::endl;

    // Show status
    if(entry % 1000000 == 0 && systName=="nominal") std::cout << "Entry = " << entry << " of " << entries << std::endl;

    // Filling Weight
    weight = xs*eff*mcEventWeight/Denominator;

    if(m_debug){
      std::cout << "mcEventWeight: " << mcEventWeight << std::endl;
      std::cout << "xs: " << xs << std::endl;
      std::cout << "eff: " << eff << std::endl;
      std::cout << "Denominator: " << Denominator << std::endl;
      std::cout << "mcEventWeight*SampleWeight: " << xs*eff*mcEventWeight/Denominator << std::endl;
      std::cout << "FinalWeight: " << weight << std::endl;
    }

    /*if(Bootstrap){
      // Generate Poisson-distributed random numbers. Set seed based on run/event #
        fGenerator->Generate(runNumber, mcEventNumber, runNumber);
    }*/

    //-----------------
    // Event Selection
    //-----------------

    int nJets = Jet_pt->size();
    int nTruthJets = TruthJet_pt->size();

    // At least one jet
    if(nJets<1 || nTruthJets<1) continue; // skip event
    
    //----------------------
    // Remove pileup events
    //----------------------
    // (for lower slices this cut prevents the leading jets from being pileup jets)
    if(nJets>1){
      double pTavg = ( Jet_pt->at(0) + Jet_pt->at(1) )*0.5;
      if( TruthJet_pt->size() == 0 || (pTavg/TruthJet_pt->at(0)>1.4) ){
        continue; // skip event
      }
    }

    //-----------------
    // Event Cleaning 
    //-----------------
    if(m_debug) std::cout << "Jet Cleaning" << std::endl;
    // Removing events only due to LooseBad
    // If jets is pileup the event is kept
    for(int j=0;j<nJets;++j){
      if(Jet_clean_passLooseBad->at(j)!=1){ // jet not passing LooseBad criteria
        should_skip = true; // Skip event
      }
    }
    if(should_skip){
      Skippedevents_Cleaning++;	    
      continue; // skip event due to a non clean jet
    }

    //---------------------------
    // Selection of Jets
    //---------------------------

    if(m_debug) std::cout << "Select Jets" << std::endl;

    // Loop over jets
    for(int k=0; k<nJets; ++k){
      // Apply jet selection cuts
      if(Jet_pt->at(k)>ptMin && fabs(Jet_eta->at(k))<detEtaMax) h_pT->Fill(Jet_pt->at(k),weight);
    }
    
  } // End: Loop over entries

  //------------------------------------
  // Saving Histogramas into a ROOT file
  //------------------------------------
  
  if(systName=="nominal") std::cout << "Saving hists into a ROOT file..." << std::endl;

  // Opening output file
  tout = new TFile(outputFile,"recreate");
  if(systName=="nominal") std::cout << "output file: " << outputFile << std::endl;
  tout->cd();

  // Writing histograms
  h_pT->Write();

  tout->Close();
    
  if(systName=="nominal") std::cout << ">>>>>> Done! <<<<<<" << std::endl;

  return 0;

}




/*
 *
 *
 * Personal Functions
 *
 *
 *
 * */


void EnableBranches(){

  tree->SetBranchStatus("*",0); //disable all branches
  tree->SetBranchStatus("mcChannelNumber",1);
  tree->SetBranchStatus("mcEventNumber",1);
  tree->SetBranchStatus("mcEventWeight",1);
  tree->SetBranchStatus("jet_pt",1);
  tree->SetBranchStatus("jet_eta",1);
  tree->SetBranchStatus("jet_phi",1);
  tree->SetBranchStatus("jet_E",1);
  tree->SetBranchStatus("jet_clean_passLooseBad",1);
  tree->SetBranchStatus("truthJet_pt",1);
  tree->SetBranchStatus("truthJet_eta",1);
  tree->SetBranchStatus("truthJet_phi",1);
  tree->SetBranchStatus("truthJet_E",1);

}//END: EnableBranches()

void SetBranchAddress(){
  // Event
  tree->SetBranchAddress("mcChannelNumber",&runNumber);
  tree->SetBranchAddress("mcEventNumber",&mcEventNumber);
  tree->SetBranchAddress("mcEventWeight",&mcEventWeight);
  // Reco Jets
  tree->SetBranchAddress("jet_pt",&Jet_pt);
  tree->SetBranchAddress("jet_eta",&Jet_eta);
  tree->SetBranchAddress("jet_E",&Jet_E);
  tree->SetBranchAddress("jet_phi",&Jet_phi);
  tree->SetBranchAddress("jet_clean_passLooseBad",&Jet_clean_passLooseBad);
  // Truth Jets
  tree->SetBranchAddress("truthJet_pt",&TruthJet_pt);
  tree->SetBranchAddress("truthJet_eta",&TruthJet_eta);
  tree->SetBranchAddress("truthJet_E",&TruthJet_E);
  tree->SetBranchAddress("truthJet_phi",&TruthJet_phi);

}//END: SetBranchAddress()

void BookHistograms(){
  // jet pT Distributions

  h_pT = new TH1D("j_pT","",npTavgBins,pTavgBins);
  h_pT->Sumw2();

  //if(Bootstrap){fGenerator = new BootstrapGenerator("Generator", "Generator", 0);}

}//END: BookHistograms()

