#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <sstream>
#include <utility>
#include <math.h>

#include <TROOT.h>
#include "TTree.h"
#include "TChain.h"
#include "TMath.h"
#include "TFile.h"
#include "TProfile.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLorentzVector.h"
#include "TGraphAsymmErrors.h"
#include "TStopwatch.h"

#include "global.h"

//#include "../../BootstrapGenerator/BootstrapGenerator/TH2DBootstrap.h"
//#include "../../BootstrapGenerator/BootstrapGenerator/TH1DBootstrap.h"
//#include "../../BootstrapGenerator/BootstrapGenerator/BootstrapGenerator.h"

TString PATH;
TString DataoutputFile;
TString MCinputFile;
TString MCoutputFile;
TString JZSlice;
TString JetCollection;
TString version;

const int nAsymmBins = 480;
const int nEtaBins = 90;
const int nCorrFactorsBins = 1000;

// Input tree
TTree *tree;
TFile *ft;
TFile *tout;

//------------
// Histograms
//------------

TH1D* h_pT;
  
struct prop {
    TLorentzVector Sel_Jet;
    bool           Used;
    bool           Iso;
    double         DetectorEta;
    double         JVT;
} ;
std::vector<prop> Selected_RscanJets;  
std::vector<prop> Selected_RefJets;  

struct prop_sel {
    TLorentzVector Sel_JetRef;
    TLorentzVector Sel_JetRscan;
    double         dR;
    double         detEta_Ref;  
    double         detEta_Rscan;  
} ;
std::vector<prop_sel> Selected_matched_Jets;

double Radius;
int ptmin; 

//-----------
// Variables
//-----------

// Event Info
int runNumber;
Long64_t eventNumber;
int mcEventNumber;
float mcEventWeight;
int lumiBlock;
int bcid;
int NPV;
float mu;
float actualMu;
double rho;
float wgt;
float weight_pileup;

TLorentzVector mu1, mu2, Z0;


// Reco Jets
int nJets; 
std::vector<float> *Jet_pt = 0;
std::vector<float> *Jet_phi = 0;
std::vector<float> *Jet_eta = 0;
std::vector<float> *Jet_E = 0;
std::vector<int>   *Jet_clean_passLooseBad = 0;

// Truth Jets
int nTruthRefJets;
std::vector<float> *TruthJet_pt = 0;
std::vector<float> *TruthJet_eta = 0;
std::vector<float> *TruthJet_phi = 0;
std::vector<float> *TruthJet_E = 0;

// for bootstrap
//Int_t nreplica = 100;
//BootstrapGenerator *fGenerator;

class iJet : public TLorentzVector {
  private:
    double detectorEta;
    int    passLooseBad;
  public:
    inline double getDetectorEta(){return detectorEta;}
    inline void   setDetectorEta(double eta){detectorEta = eta;}
    inline void   setCleaning(float pass){passLooseBad = pass;}
    inline float  getCleaning(){return passLooseBad;}
};

  
