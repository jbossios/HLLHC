#!/usr/bin/python
import os, sys
from ROOT import *

MC15c = False
HLLHC_optimistic   = False
HLLHC_conservative = True

systs_MC15c = [
#  "nominal",
  "JET_EffectiveNP_1__1up",
  "JET_EffectiveNP_1__1down",
  "JET_EffectiveNP_2__1up",
  "JET_EffectiveNP_2__1down",
  "JET_EffectiveNP_3__1up",
  "JET_EffectiveNP_3__1down",
  "JET_EffectiveNP_4__1up",
  "JET_EffectiveNP_4__1down",
  "JET_EffectiveNP_5__1up",
  "JET_EffectiveNP_5__1down",
  "JET_EffectiveNP_6__1up",
  "JET_EffectiveNP_6__1down",
  "JET_EffectiveNP_7__1up",
  "JET_EffectiveNP_7__1down",
  "JET_EffectiveNP_8restTerm__1up",
  "JET_EffectiveNP_8restTerm__1down",
  "JET_EtaIntercalibration_Modelling__1up",
  "JET_EtaIntercalibration_Modelling__1down",
  "JET_EtaIntercalibration_NonClosure__1up",
  "JET_EtaIntercalibration_NonClosure__1down",
  "JET_EtaIntercalibration_TotalStat__1up",
  "JET_EtaIntercalibration_TotalStat__1down",
  "JET_Flavor_Composition__1up",
  "JET_Flavor_Composition__1down",
  "JET_Flavor_Response__1up",
  "JET_Flavor_Response__1down",
  "JET_Pileup_OffsetMu__1up",
  "JET_Pileup_OffsetMu__1down",
  "JET_Pileup_OffsetNPV__1up",
  "JET_Pileup_OffsetNPV__1down",
  "JET_Pileup_PtTerm__1up",
  "JET_Pileup_PtTerm__1down",
  "JET_Pileup_RhoTopology__1up",
  "JET_Pileup_RhoTopology__1down",
  "JET_JER_SINGLE_NP__1up",
  "JET_PunchThrough_MC15__1up",
  "JET_PunchThrough_MC15__1down",
  "JET_SingleParticle_HighPt__1up",
  "JET_SingleParticle_HighPt__1down",
]

systs_HLLHC_optimistic=[ # HLLHC optimistic
#  "nominal", # DONE
#  "JET_EffectiveNP_1__1up", # DONE
#  "JET_EffectiveNP_1__1down", # DONE
#  "JET_EffectiveNP_2__1up", # DONE
#  "JET_EffectiveNP_2__1down", # DONE
#  "JET_EffectiveNP_3__1up", # DONE
#  "JET_EffectiveNP_3__1down", # DONE
#  "JET_EffectiveNP_4__1up", # DONE
#  "JET_EffectiveNP_4__1down", # DONE
#  "JET_EffectiveNP_5__1up", # DONE
#  "JET_EffectiveNP_5__1down", # DONE
#  "JET_EffectiveNP_6__1up", # DONE
#  "JET_EffectiveNP_6__1down", # DONE
#  "JET_EffectiveNP_7__1up", # DONE
#  "JET_EffectiveNP_7__1down", # DONE
#  "JET_EffectiveNP_8restTerm__1up", # DONE
#  "JET_EffectiveNP_8restTerm__1down", # DONE
#  "JET_Flavor_Response__1up", # DONE
#  "JET_Flavor_Response__1down", # DONE
#  "JET_Pileup_RhoTopology__1up", # DONE
#  "JET_Pileup_RhoTopology__1down", # DONE
#  "JET_JER_SINGLE_NP__1up", # DONE
]

systs_HLLHC_conservative=[ # HLLHC conservative
  "nominal", # DONE
  "JET_EffectiveNP_1__1up", # DONE
  "JET_EffectiveNP_1__1down", # DONE
  "JET_EffectiveNP_2__1up", # DONE
  "JET_EffectiveNP_2__1down", # DONE
  "JET_EffectiveNP_3__1up", # DONE
  "JET_EffectiveNP_3__1down", # DONE
  "JET_EffectiveNP_4__1up", # DONE
  "JET_EffectiveNP_4__1down", # DONE
  "JET_EffectiveNP_5__1up", # DONE
  "JET_EffectiveNP_5__1down", # DONE
  "JET_EffectiveNP_6__1up", # DONE
  "JET_EffectiveNP_6__1down", # DONE
  "JET_EffectiveNP_7__1up", # DONE
  "JET_EffectiveNP_7__1down", # DONE
  "JET_EffectiveNP_8restTerm__1up", # DONE
  "JET_EffectiveNP_8restTerm__1down", # DONE
  "JET_EtaIntercalibration_Modelling__1up",
  "JET_EtaIntercalibration_Modelling__1down",
  "JET_Flavor_Response__1up", # DONE
  "JET_Flavor_Response__1down", # DONE
  "JET_Pileup_RhoTopology__1up", # DONE
  "JET_Pileup_RhoTopology__1down", # DONE
  "JET_JER_SINGLE_NP__1up",
]

##########################################################
## DO NOT MODIFY
##########################################################

# Protection
if HLLHC_optimistic and HLLHC_conservative:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if HLLHC_conservative and MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if HLLHC_optimistic and MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if not HLLHC_optimistic and not HLLHC_conservative and not MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)

Systs = []
if HLLHC_conservative:
  Systs = systs_HLLHC_conservative
elif HLLHC_optimistic:
  Systs = systs_HLLHC_optimistic
elif MC15c:
  Systs = systs_MC15c
command = ""
for syst in Systs:
  command += "hadd Results/"
  if MC15c:
    command += "MC15c/"
  elif HLLHC_optimistic:
    command += "HLLHC_optimistic/"
  elif HLLHC_conservative:
    command += "HLLHC_conservative/"
  command += syst
  command += ".root "
  command += "Results/"
  if MC15c:
    command += "MC15c/"
  elif HLLHC_optimistic:
    command += "HLLHC_optimistic/"
  elif HLLHC_conservative:
    command += "HLLHC_conservative/"
  command += syst
  command += "_*.root"
  command += " && "

command = command[:-3]
command += "&"
print command
os.system(command)
















