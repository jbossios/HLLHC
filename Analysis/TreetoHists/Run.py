#!/usr/bin/python
import os, sys
from ROOT import *
param=sys.argv

MC15c = False
HLLHC_optimistic   = False
HLLHC_conservative = True
Debug = False # More verbosity and run over only 10 events

systs_MC15c = [ # ALL DONE
#  "nominal", # DONE
#  "EffectiveNP_1__1up", # DONE
#  "EffectiveNP_1__1down", # DONE
#  "EffectiveNP_2__1up", # DONE
#  "EffectiveNP_2__1down", # DONE
#  "EffectiveNP_3__1up", # DONE
#  "EffectiveNP_3__1down", # DONE
#  "EffectiveNP_4__1up", # DONE
#  "EffectiveNP_4__1down", # DONE
#  "EffectiveNP_5__1up", # DONE
#  "EffectiveNP_5__1down", # DONE
#  "EffectiveNP_6__1up", # DONE
#  "EffectiveNP_6__1down", # DONE
#  "EffectiveNP_7__1up", # DONE
#  "EffectiveNP_7__1down", # DONE
#  "EffectiveNP_8restTerm__1up", # DONE
#  "EffectiveNP_8restTerm__1down", # DONE
#  "EtaIntercalibration_Modelling__1up", # DONE
#  "EtaIntercalibration_Modelling__1down", # DONE
#  "EtaIntercalibration_NonClosure__1up", # DONE
#  "EtaIntercalibration_NonClosure__1down", # DONE
#  "EtaIntercalibration_TotalStat__1up", # DONE
#  "EtaIntercalibration_TotalStat__1down", # DONE
#  "Flavor_Composition__1up", # DONE
#  "Flavor_Composition__1down", # DONE
#  "Flavor_Response__1up", # DONE
#  "Flavor_Response__1down", # DONE
#  "Pileup_OffsetMu__1up", # DONE
#  "Pileup_OffsetMu__1down", # DONE
#  "Pileup_OffsetNPV__1up", # DONE
#  "Pileup_OffsetNPV__1down", # DONE
#  "Pileup_PtTerm__1up", # DONE
#  "Pileup_PtTerm__1down", # DONE
#  "Pileup_RhoTopology__1up", # DONE
#  "Pileup_RhoTopology__1down", # DONE
#  "PunchThrough_MC15__1up", # DONE
#  "PunchThrough_MC15__1down", # DONE
#  "SingleParticle_HighPt__1up", # DONE
#  "SingleParticle_HighPt__1down", # DONE
#  "JER_SINGLE_NP__1up", # DONE
]

systs_HLLHC_optimistic=[ # HLLHC optimistic
#  "nominal", # DONE
#  "EffectiveNP_1__1up", #   DONE
#  "EffectiveNP_1__1down", # DONE
#  "EffectiveNP_2__1up", #   DONE
#  "EffectiveNP_2__1down", # DONE
#  "EffectiveNP_3__1up", #   DONE
#  "EffectiveNP_3__1down", # DONE
#  "EffectiveNP_4__1up", #   DONE
#  "EffectiveNP_4__1down", # DONE
#  "EffectiveNP_5__1up", #   DONE
#  "EffectiveNP_5__1down", # DONE
#  "EffectiveNP_6__1up", #   DONE
#  "EffectiveNP_6__1down", # DONE
#  "EffectiveNP_7__1up", #   DONE
#  "EffectiveNP_7__1down", # DONE
#  "EffectiveNP_8restTerm__1up", # DONE
#  "EffectiveNP_8restTerm__1down", # DONE
#  "Flavor_Response__1up", # DONE
#  "Flavor_Response__1down", # DONE
#  "Pileup_RhoTopology__1up", # DONE
#  "Pileup_RhoTopology__1down", # DONE
#  "JER_SINGLE_NP__1up", # DONE
]

systs_HLLHC_conservative=[ # HLLHC conservative
#  "nominal", #  DONE
#  "EffectiveNP_1__1up", #   DONE
#  "EffectiveNP_1__1down", # DONE
#  "EffectiveNP_2__1up", #   DONE
#  "EffectiveNP_2__1down", # DONE
#  "EffectiveNP_3__1up", #   DONE
#  "EffectiveNP_3__1down", # DONE
#  "EffectiveNP_4__1up", #   DONE
#  "EffectiveNP_4__1down", # DONE
#  "EffectiveNP_5__1up", #   DONE
#  "EffectiveNP_5__1down", # DONE
#  "EffectiveNP_6__1up", #   DONE
#  "EffectiveNP_6__1down", # DONE
#  "EffectiveNP_7__1up", #   DONE
#  "EffectiveNP_7__1down", # DONE
#  "EffectiveNP_8restTerm__1up", # DONE
#  "EffectiveNP_8restTerm__1down", # DONE
#  "EtaIntercalibration_Modelling__1up", # DONE
#  "EtaIntercalibration_Modelling__1down", # DONE
#  "Flavor_Response__1up", # DONE
#  "Flavor_Response__1down", # DONE
#  "Pileup_RhoTopology__1up", # DONE
#  "Pileup_RhoTopology__1down", # DONE
  "JER_SINGLE_NP__1up", # 
]

MC15cTTrees = [
  "/eos/atlas/user/j/jbossios/HLLHC/TTrees/MC15c/user.jbossios.18_08_2017_MC15cPythia.JZ1_tree.root/",
  "/eos/atlas/user/j/jbossios/HLLHC/TTrees/MC15c/user.jbossios.18_08_2017_MC15cPythia.JZ2_tree.root/",
  "/eos/atlas/user/j/jbossios/HLLHC/TTrees/MC15c/user.jbossios.18_08_2017_MC15cPythia.JZ3_tree.root/",
  "/eos/atlas/user/j/jbossios/HLLHC/TTrees/MC15c/user.jbossios.18_08_2017_MC15cPythia.JZ4_tree.root/",
  "/eos/atlas/user/j/jbossios/HLLHC/TTrees/MC15c/user.jbossios.18_08_2017_MC15cPythia.JZ5_tree.root/",
  "/eos/atlas/user/j/jbossios/HLLHC/TTrees/MC15c/user.jbossios.18_08_2017_MC15cPythia.JZ6_tree.root/",
  "/eos/atlas/user/j/jbossios/HLLHC/TTrees/MC15c/user.jbossios.18_08_2017_MC15cPythia.JZ7_tree.root/",
]

#HLLHCTTrees_optimistic = [ # JER buggy
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Optimistic/user.jbossios.22_08_2018_HLLHC_Optimistic_v2.JZ1_tree.root/",
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Optimistic/user.jbossios.23_08_2018_HLLHC_Optimistic_v2.JZ2_tree.root/",
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Optimistic/user.jbossios.23_08_2018_HLLHC_Optimistic_v2.JZ3_tree.root/",
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Optimistic/user.jbossios.23_08_2018_HLLHC_Optimistic_v2.JZ4_tree.root/",
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Optimistic/user.jbossios.23_08_2018_HLLHC_Optimistic.JZ5_tree.root/",
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Optimistic/user.jbossios.23_08_2018_HLLHC_Optimistic_v2.JZ6_tree.root/",
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Optimistic/user.jbossios.23_08_2018_HLLHC_Optimistic_v2.JZ7_tree.root/",
#]

HLLHCTTrees_optimistic = [ # JER bugFix
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Optimistic/user.jbossios.29_08_2018_HLLHC_Optimistic_JERbugFix.JZ1_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Optimistic/user.jbossios.29_08_2018_HLLHC_Optimistic_JERbugFix.JZ2_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Optimistic/user.jbossios.29_08_2018_HLLHC_Optimistic_JERbugFix.JZ3_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Optimistic/user.jbossios.29_08_2018_HLLHC_Optimistic_JERbugFix.JZ4_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Optimistic/user.jbossios.29_08_2018_HLLHC_Optimistic_JERbugFix.JZ5_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Optimistic/user.jbossios.29_08_2018_HLLHC_Optimistic_JERbugFix.JZ6_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Optimistic/user.jbossios.29_08_2018_HLLHC_Optimistic_JERbugFix.JZ7_tree.root/",
]

#HLLHCTTrees_conservative = [ # JER buggy
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Conservative/user.jbossios.26_08_2018_HLLHC_Conservative.JZ1_tree.root/",
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Conservative/user.jbossios.26_08_2018_HLLHC_Conservative.JZ2_tree.root/",
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Conservative/user.jbossios.26_08_2018_HLLHC_Conservative.JZ3_tree.root/",
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Conservative/user.jbossios.26_08_2018_HLLHC_Conservative.JZ4_tree.root/",
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Conservative/user.jbossios.26_08_2018_HLLHC_Conservative.JZ5_tree.root/",
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Conservative/user.jbossios.26_08_2018_HLLHC_Conservative.JZ6_tree.root/",
#  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC/Conservative/user.jbossios.26_08_2018_HLLHC_Conservative.JZ7_tree.root/",
#]

HLLHCTTrees_conservative = [ # JER bugfix
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Conservative/user.jbossios.29_08_2018_HLLHC_Conservative_JERbugFix.JZ1_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Conservative/user.jbossios.29_08_2018_HLLHC_Conservative_JERbugFix.JZ2_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Conservative/user.jbossios.29_08_2018_HLLHC_Conservative_JERbugFix.JZ3_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Conservative/user.jbossios.29_08_2018_HLLHC_Conservative_JERbugFix.JZ4_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Conservative/user.jbossios.29_08_2018_HLLHC_Conservative_JERbugFix.JZ5_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Conservative/user.jbossios.29_08_2018_HLLHC_Conservative_JERbugFix.JZ6_tree.root/",
  "/eos/user/j/jbossios/HLLHC/TTrees/HLLHC_JERbugFix/Conservative/user.jbossios.29_08_2018_HLLHC_Conservative_JERbugFix.JZ7_tree.root/",
]

####################################################################################
# DO NOT MODIFY
####################################################################################

# Protection
if HLLHC_optimistic and HLLHC_conservative:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if HLLHC_conservative and MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if HLLHC_optimistic and MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if not HLLHC_optimistic and not HLLHC_conservative and not MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)

ComputeSampleWeights = True

command = ""
Systs = []
if HLLHC_conservative:
  Systs = systs_HLLHC_conservative
elif HLLHC_optimistic:
  Systs = systs_HLLHC_optimistic
elif MC15c:
  Systs = systs_MC15c
for syst in Systs:
  print "Systematic: "+syst	
  List = []
  if HLLHC_conservative:
    List = HLLHCTTrees_conservative
  if HLLHC_optimistic:
    List = HLLHCTTrees_optimistic
  if MC15c:
    List = MC15cTTrees
  #command = ""
  for path in List:
    for File in os.listdir(path):
      # Check if root file contains a tree
      iFile = TFile.Open(path+File,"read")
      if not iFile:
        print "Error: file not found"	      
	sys.exit(0)
      tree = TTree()
      TDir = iFile.GetDirectory("TreeAlgo")
      if not TDir:
        continue # metadata file
      if syst is "nominal":
        tree = TDir.Get(syst)
      else:
        tree = TDir.Get("JET_"+syst)
      if not tree:
        continue
      # Run if exist the tree
      command += "TreetoHists "
      if MC15c:
	command += " --MC15c=TRUE"
      if HLLHC_optimistic:
	command += " --HLLHCOptimistic=TRUE"
      if HLLHC_conservative:
	command += " --HLLHCConservative=TRUE"
      command += " --syst="
      command += syst
      if Debug:
        command += " --debug=TRUE"
      command += " --pathUSER="
      command += path
      command += " --inputFileUSER="
      command += File
      command += " && "
  #command = command[:-3]
  #command += "&"
  #print command
  #os.system(command)

command = command[:-3]
command += "&"
print command
os.system(command)
