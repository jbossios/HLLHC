##########################################
# Author: Jona Bossio (jbossios@cern.ch)
##########################################

from ROOT import *
import os,sys

CMEs = ["14","27"]

PDFs = [
  "NNPDF31_nlo_pdfas",
  "PDF4LHC15_nnlo_hllhc_scen1",
  "HERAPDF20_NLO_EIG",
  "ABMP16_5_nnlo",
  "MMHT2014nlo68clas118",
]

Colors = [kOrange, kRed, kBlue+2, kGreen+2, kMagenta]

yBins   = ["00_05","05_10","10_15","15_20","20_25","25_30"]
RapBins = ["|y|<0.5","0.5#leq|y|<1.0","1.0#leq|y|<1.5","1.5#leq|y|<2.0","2.0#leq|y|<2.5","2.5#leq|y|<3.0"]

#####################################################################
## DO NOT MODIFY
#####################################################################

gROOT.SetBatch(True)  # so does not pop up plots!

# ATLAS style
gROOT.LoadMacro("/home/jbossios/cern/AtlasStyle/AtlasStyle.C")
SetAtlasStyle()

Bins = [0,5]

# Loop over CMEs
for cme in CMEs:
  PATH = "/home/jbossios/cern/HLLHC/Inputs/Pavel/"+cme+"TeV/uncert/"
  # Get total uncertainty band
  uncertFileName = PATH+"theoryIJET_CT14nlo_antiKtR04.root"
  uncertFile = TFile.Open(uncertFileName,"READ") 
  if not uncertFile:
    print "File with uncertainty band not found, exiting"
    sys.exit(0)
  # Get PDF4LHC File
  uncertFileName2 = PATH+"theoryIJET_PDF4LHC15_nnlo_hllhc_scen1_antiKtR04.root"
  uncertFile2 = TFile.Open(uncertFileName2,"READ") 
  if not uncertFile2:
    print "File with uncertainty band for PDF4LHC not found, exiting"
    sys.exit(0)
  # Loop over rapidity bins
  #for ybin in range(0,6):
  for ybin in Bins:
    xmin = 99.
    xmax = 0.
    ymin = 0.2
    ymax = 1.6
    if cme=="14" and ybin==0:
      xmax = 8000.
    elif cme=="27" and ybin==0:
      xmax = 14000.
    elif cme=="14" and ybin==5:
      xmax = 1030.
    elif cme=="27" and ybin==5:
      xmax = 2000.
    print "yBin: "+str(ybin)
    can = TCanvas()
    can.SetLogx()
    outPDF = "../Plots/TheoryRatios/IJXS_"+cme+"TeV_PDFuncert_ybin"+str(ybin)+".pdf"
    can.Print(outPDF+"[")
    legend = TLegend(0.2,0.7,0.6,0.9) # x1,y1,x2,y2
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.SetLineColor(0)
    legend.SetBorderSize(0)
    legend.SetTextFont(42)
    # TMultiGraph
    mg = TMultiGraph()
    # Plot uncertainty band
    uncertBandName = "g_pdfError_CT14nlo_"+yBins[ybin]
    print "Geting uncertainty band: "+uncertBandName
    uncertBand = uncertFile.Get(uncertBandName)
    if not uncertBand:
      print "Uncertainty band not found, exiting"
      sys.exit(0)
    uncertBand.SetFillColorAlpha(kBlack,0.3)
    #uncertBand.SetFillColor(kBlack)
    #uncertBand.SetFillStyle(3005)
    #uncertBand.SetFillColor(kBlack)
    uncertBand.SetLineColor(0)
    uncertBand.GetXaxis().SetMoreLogLabels()
    uncertBand.GetXaxis().SetRangeUser(xmin,xmax)
    uncertBand.GetXaxis().SetTitle("#it{p}_{T} [GeV]")
    uncertBand.GetYaxis().SetTitle("PDF uncertainty")
    uncertBand.SetMinimum(ymin)
    uncertBand.SetMaximum(ymax)
    #uncertBand.Draw("a2")
    mg.Add(uncertBand,"a2")
    #mg.Draw("AC")
    legend.AddEntry(uncertBand,"CT14","f")
    # Plot PDF uncertainty band for PDF4LHC
    uncertBandName2 = "g_pdfError_PDF4LHC15_nnlo_hllhc_scen1_"+yBins[ybin]
    print "Geting uncertainty band: "+uncertBandName2
    uncertBand2 = uncertFile2.Get(uncertBandName2)
    if not uncertBand2:
      print "Uncertainty band 2 not found, exiting"
      sys.exit(0)
    #uncertBand2.SetFillColorAlpha(kRed,0.3)
    uncertBand2.SetFillColor(kRed)
    uncertBand2.SetFillStyle(3004)
    #uncertBand2.SetFillColor(kRed)
    uncertBand2.SetLineColor(0)
    uncertBand2.SetMinimum(ymin)
    uncertBand2.SetMaximum(ymax)
    #uncertBand2.Draw("a2")
    mg.Add(uncertBand2,"e2")
    #mg.Draw("C")
    legend.AddEntry(uncertBand2,"PDF4LHC HL-LHC scenario 1","f")
    # Plot ratios
    # Loop over PDFs
    count = 0
    mg.Draw() 
    legend.Draw("same")
    TextBlock = TLatex(0.21,0.65,"#scale[0.8]{"+RapBins[ybin]+"}")
    TextBlock.SetNDC()
    TextBlock.Draw("same")
    can.Print(outPDF)
    can.Print(outPDF+"]")
  
     


