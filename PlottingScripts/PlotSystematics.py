#!/usr/bin/python
import os, sys
from ROOT import *
from array import array
param=sys.argv

gROOT.SetBatch(True)  # so does not pop up plots!
gROOT.LoadMacro("AtlasStyle.C")
SetAtlasStyle()

YMin = -0.3
YMax = 0.4

MC15c = True
HLLHC_optimistic   = False
HLLHC_conservative = False
Debug = False # More verbosity and run over only 10 events

Colors = [8,8,kRed+2,kRed+2,kGreen+2,kGreen+2,kMagenta+1,kMagenta+1,kOrange+2,kOrange+2,kAzure+10,kAzure+10,kBlue+2,kBlue+2]

systs_MC15c_1 = [
  "JET_EffectiveNP_1__1up", # DONE
  "JET_EffectiveNP_1__1down", # DONE
  "JET_EffectiveNP_2__1up", # DONE
  "JET_EffectiveNP_2__1down", # DONE
  "JET_EffectiveNP_3__1up", # DONE
  "JET_EffectiveNP_3__1down", # DONE
  "JET_EffectiveNP_4__1up", # DONE
  "JET_EffectiveNP_4__1down", # DONE
  "JET_EffectiveNP_5__1up", # DONE
  "JET_EffectiveNP_5__1down", # DONE
  "JET_EffectiveNP_6__1up", # DONE
  "JET_EffectiveNP_6__1down", # DONE
  "JET_EffectiveNP_7__1up", # DONE
  "JET_EffectiveNP_7__1down", # DONE
]

systs_MC15c_2 = [
  "JET_EffectiveNP_8restTerm__1up", # DONE
  "JET_EffectiveNP_8restTerm__1down", # DONE
  "JET_EtaIntercalibration_Modelling__1up", # DONE
  "JET_EtaIntercalibration_Modelling__1down", # DONE
  "JET_EtaIntercalibration_NonClosure__1up", # DONE
  "JET_EtaIntercalibration_NonClosure__1down", # DONE
  "JET_EtaIntercalibration_TotalStat__1up", # DONE
  "JET_EtaIntercalibration_TotalStat__1down", # DONE
  "JET_Flavor_Composition__1up", # DONE
  "JET_Flavor_Composition__1down", # DONE
  "JET_Flavor_Response__1up", # DONE
  "JET_Flavor_Response__1down", # DONE
]

systs_MC15c_3 = [
  "JET_Pileup_OffsetMu__1up", # DONE
  "JET_Pileup_OffsetMu__1down", # DONE
  "JET_Pileup_OffsetNPV__1up", # DONE
  "JET_Pileup_OffsetNPV__1down", # DONE
  "JET_Pileup_PtTerm__1up", # DONE
  "JET_Pileup_PtTerm__1down", # DONE
  "JET_Pileup_RhoTopology__1up", # DONE
  "JET_Pileup_RhoTopology__1down", # DONE
  "JET_PunchThrough_MC15__1up", # DONE
  "JET_PunchThrough_MC15__1down", # DONE
  "JET_SingleParticle_HighPt__1up", # DONE
  "JET_SingleParticle_HighPt__1down", # DONE
  "JET_JER_SINGLE_NP__1up", # DONE
]

systs_HLLHC_optimistic=[ # HLLHC optimistic
#  "JET_EffectiveNP_1__1up", # DONE
#  "JET_EffectiveNP_1__1down", # DONE
#  "JET_EffectiveNP_2__1up",
#  "JET_EffectiveNP_2__1down",
#  "JET_EffectiveNP_3__1up",
#  "JET_EffectiveNP_3__1down",
#  "JET_EffectiveNP_4__1up",
#  "JET_EffectiveNP_4__1down",
#  "JET_EffectiveNP_5__1up",
#  "JET_EffectiveNP_5__1down",
#  "JET_EffectiveNP_6__1up",
#  "JET_EffectiveNP_6__1down",
#  "JET_EffectiveNP_7__1up",
#  "JET_EffectiveNP_7__1down",
#  "JET_EffectiveNP_8restTerm__1up",
#  "JET_EffectiveNP_8restTerm__1down",
#  "JET_Flavor_Response__1up",
#  "JET_Flavor_Response__1down",
#  "JET_Pileup_RhoTopology__1up",
#  "JET_Pileup_RhoTopology__1down",
#  "JET_JER_SINGLE_NP__1up",
]

systs_HLLHC_conservative=[ # HLLHC conservative FIXME
  "JET_EffectiveNP_1__1up",
  "JET_EffectiveNP_1__1down",
  "JET_EffectiveNP_2__1up",
  "JET_EffectiveNP_2__1down",
  "JET_EffectiveNP_3__1up",
  "JET_EffectiveNP_3__1down",
  "JET_EffectiveNP_4__1up",
  "JET_EffectiveNP_4__1down",
  "JET_EffectiveNP_5__1up",
  "JET_EffectiveNP_5__1down",
  "JET_EffectiveNP_6__1up",
  "JET_EffectiveNP_6__1down",
  "JET_EffectiveNP_7__1up",
  "JET_EffectiveNP_7__1down",
  "JET_EffectiveNP_8restTerm__1up",
  "JET_EffectiveNP_8restTerm__1down",
  "JET_Flavor_Response__1up",
  "JET_Flavor_Response__1down",
  "JET_Pileup_RhoTopology__1up",
  "JET_Pileup_RhoTopology__1down",
  "JET_JER_SINGLE_NP__1up",
]


####################################################################################
# DO NOT MODIFY
####################################################################################

# Protection
if HLLHC_optimistic and HLLHC_conservative:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if HLLHC_conservative and MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if HLLHC_optimistic and MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if not HLLHC_optimistic and not HLLHC_conservative and not MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)

Systs_1 = []
Systs_2 = []
Systs_3 = []
if HLLHC_conservative:
  Systs = systs_HLLHC_conservative
elif HLLHC_optimistic:
  Systs = systs_HLLHC_optimistic
elif MC15c:
  Systs_1 = systs_MC15c_1
  Systs_2 = systs_MC15c_2
  Systs_3 = systs_MC15c_3

InputFileName = ""
if HLLHC_conservative:
  InputFileName += "HLLHC_conservative"
elif HLLHC_optimistic:
  InputFileName += "HLLH_optimistic"
elif MC15c:
  InputFileName += "MC15c"
InputFileName += ".root"
InputFile = TFile.Open(InputFileName)
if not InputFile:
    print "InputFile not found, exiting"
    sys.exit(0)


# Get Total up and down systematics
npTavgBins = 46;
pTavgBins = [15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941., 6200., 7780., 9763.]
hTotal_up = TH1D("Total","",npTavgBins,array('d',pTavgBins))
# Loop over pT bins
for bin in range(1,npTavgBins+1):
  TotalUncertainty = 0;
  for syst in Systs_1:
    if "1up" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print "VarHist not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
  for syst in Systs_2:
    if "1up" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print "VarHist not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
  for syst in Systs_3:
    if "1up" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print "VarHist not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
  TotalUncertainty = TMath.Sqrt(TotalUncertainty)
  #print TotalUncertainty
  hTotal_up.SetBinContent(bin,TotalUncertainty)
  hTotal_up.SetBinError(bin,0)
hTotal_down = TH1D("Total","",npTavgBins,array('d',pTavgBins))
# Loop over pT bins
for bin in range(1,npTavgBins+1):
  TotalUncertainty = 0;
  for syst in Systs_1:
    if "1down" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print "VarHist not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
  for syst in Systs_2:
    if "1down" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print "VarHist not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
  for syst in Systs_3:
    if "1down" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print "VarHist not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
    if "SINGLE" in syst:
      VarHist = InputFile.Get(syst)
      if not VarHist:
        print "VarHist not found, exiting"
        sys.exit(0)
      Uncertainty = VarHist.GetBinContent(bin)
      TotalUncertainty += Uncertainty * Uncertainty
  TotalUncertainty = TMath.Sqrt(TotalUncertainty)
  #print TotalUncertainty
  hTotal_down.SetBinContent(bin,TotalUncertainty)
  hTotal_down.SetBinError(bin,0)

# First Set 
OutPDF = ""
if MC15c:
  OutPDF += "MC15c"
if HLLHC_conservative:
  OutPDF += "HLLHC_conservative"
if HLLHC_optimistic:
  OutPDF += "HLLHC_optimistic"
OutPDF += "_1.pdf"
canvas = TCanvas("","",800,800)
canvas.Print(OutPDF+"[")
canvas.SetLogx()
legend = TLegend(0.2,0.6,0.6,0.93) #x1,y1,x2,y2
counter = 0
for syst in Systs_1:
  # Get Systematic variation Histogram
  VarHist = InputFile.Get(syst)
  if not VarHist:
    print "VarHist not found, exiting"
    sys.exit(0)
  VarHist.SetMinimum(YMin)
  VarHist.SetMaximum(YMax)
  VarHist.GetXaxis().SetMoreLogLabels()
  VarHist.GetXaxis().SetRangeUser(100,3900)
  VarHist.SetLineColor(Colors[counter])
  VarHist.SetMarkerColor(Colors[counter])
  VarHist.SetMarkerSize(0.5)
  VarHist.GetXaxis().SetTitle("#it{p}_{T}")
  VarHist.GetYaxis().SetTitle("Relative Uncertainty")
  if counter== 0:
    VarHist.Draw()
  else:
    VarHist.Draw("same")
  legend.AddEntry(VarHist,syst)
  counter += 1
VarHist.Draw("same")
hTotal_up.Draw("same")
hTotal_up.SetMinimum(YMin)
hTotal_up.SetMaximum(YMax)
hTotal_up.GetXaxis().SetMoreLogLabels()
hTotal_up.GetXaxis().SetRangeUser(100,3900)
hTotal_up.SetLineColor(kBlack)
hTotal_up.SetMarkerColor(kBlack)
hTotal_up.SetMarkerSize(0.5)
hTotal_up.GetXaxis().SetTitle("#it{p}_{T}")
hTotal_up.GetYaxis().SetTitle("Relative Uncertainty")
hTotal_down.Scale(-1)
hTotal_down.Draw("same")
hTotal_down.SetMinimum(YMin)
hTotal_down.SetMaximum(YMax)
hTotal_down.GetXaxis().SetMoreLogLabels()
hTotal_down.GetXaxis().SetRangeUser(100,3900)
hTotal_down.SetLineColor(kBlack)
hTotal_down.SetMarkerColor(kBlack)
hTotal_down.SetMarkerSize(0.5)
hTotal_down.GetXaxis().SetTitle("#it{p}_{T}")
hTotal_down.GetYaxis().SetTitle("Relative Uncertainty")
legend.AddEntry(hTotal_up,"Total")
legend.Draw("same")
canvas.Print(OutPDF)
canvas.Print(OutPDF+"]")

# Second Set 
OutPDF = ""
if MC15c:
  OutPDF += "MC15c"
if HLLHC_conservative:
  OutPDF += "HLLHC_conservative"
if HLLHC_optimistic:
  OutPDF += "HLLHC_optimistic"
OutPDF += "_2.pdf"
canvas = TCanvas("","",800,800)
canvas.Print(OutPDF+"[")
canvas.SetLogx()
legend = TLegend(0.2,0.6,0.6,0.93) #x1,y1,x2,y2
counter = 0
for syst in Systs_2:
  # Get Systematic variation Histogram
  VarHist = InputFile.Get(syst)
  if not VarHist:
    print "VarHist not found, exiting"
    sys.exit(0)
  VarHist.SetMinimum(YMin)
  VarHist.SetMaximum(YMax)
  VarHist.GetXaxis().SetMoreLogLabels()
  VarHist.GetXaxis().SetRangeUser(100,3900)
  VarHist.SetLineColor(Colors[counter])
  VarHist.SetMarkerColor(Colors[counter])
  VarHist.SetMarkerSize(0.5)
  VarHist.GetXaxis().SetTitle("#it{p}_{T}")
  VarHist.GetYaxis().SetTitle("Relative Uncertainty")
  if counter== 0:
    VarHist.Draw()
  else:
    VarHist.Draw("same")
  legend.AddEntry(VarHist,syst)
  counter += 1
VarHist.Draw("same")
hTotal_up.Draw("same")
hTotal_up.SetMinimum(YMin)
hTotal_up.SetMaximum(YMax)
hTotal_up.GetXaxis().SetMoreLogLabels()
hTotal_up.GetXaxis().SetRangeUser(100,3900)
hTotal_up.SetLineColor(kBlack)
hTotal_up.SetMarkerColor(kBlack)
hTotal_up.SetMarkerSize(0.5)
hTotal_up.GetXaxis().SetTitle("#it{p}_{T}")
hTotal_up.GetYaxis().SetTitle("Relative Uncertainty")
hTotal_down.Draw("same")
hTotal_down.SetMinimum(YMin)
hTotal_down.SetMaximum(YMax)
hTotal_down.GetXaxis().SetMoreLogLabels()
hTotal_down.GetXaxis().SetRangeUser(100,3900)
hTotal_down.SetLineColor(kBlack)
hTotal_down.SetMarkerColor(kBlack)
hTotal_down.SetMarkerSize(0.5)
hTotal_down.GetXaxis().SetTitle("#it{p}_{T}")
hTotal_down.GetYaxis().SetTitle("Relative Uncertainty")
legend.AddEntry(hTotal_up,"Total")
legend.Draw("same")
canvas.Print(OutPDF)
canvas.Print(OutPDF+"]")

# Third Set 
OutPDF = ""
if MC15c:
  OutPDF += "MC15c"
if HLLHC_conservative:
  OutPDF += "HLLHC_conservative"
if HLLHC_optimistic:
  OutPDF += "HLLHC_optimistic"
OutPDF += "_3.pdf"
canvas = TCanvas("","",800,800)
canvas.Print(OutPDF+"[")
canvas.SetLogx()
legend = TLegend(0.2,0.6,0.6,0.93) #x1,y1,x2,y2
counter = 0
for syst in Systs_3:
  # Get Systematic variation Histogram
  VarHist = InputFile.Get(syst)
  if not VarHist:
    print "VarHist not found, exiting"
    sys.exit(0)
  VarHist.SetMinimum(YMin)
  VarHist.SetMaximum(YMax)
  VarHist.GetXaxis().SetMoreLogLabels()
  VarHist.GetXaxis().SetRangeUser(100,4000)
  VarHist.SetLineColor(Colors[counter])
  VarHist.SetMarkerColor(Colors[counter])
  VarHist.SetMarkerSize(0.5)
  VarHist.GetXaxis().SetTitle("#it{p}_{T}")
  VarHist.GetYaxis().SetTitle("Relative Uncertainty")
  if counter== 0:
    VarHist.Draw()
  else:
    VarHist.Draw("same")
  legend.AddEntry(VarHist,syst)
  if syst is "JET_JER_SINGLE_NP__1up":
    VarHist2 = VarHist.Clone("JET_JER_SINGLE_NP__1down")
    VarHist2.Scale(-1.)
    VarHist2.SetMinimum(YMin)
    VarHist2.SetMaximum(YMax)
    VarHist2.GetXaxis().SetMoreLogLabels()
    VarHist2.GetXaxis().SetRangeUser(100,4000)
    VarHist2.SetLineColor(Colors[counter])
    VarHist2.SetMarkerColor(Colors[counter])
    VarHist2.SetMarkerSize(0.5)
    VarHist2.GetXaxis().SetTitle("#it{p}_{T}")
    VarHist2.GetYaxis().SetTitle("Relative Uncertainty")
    VarHist2.Draw("same")
    legend.AddEntry(VarHist2,"JET_JER_SINGLE_NP__1down")
  counter += 1
VarHist.Draw("same")
hTotal_up.Draw("same")
hTotal_up.SetMinimum(YMin)
hTotal_up.SetMaximum(YMax)
hTotal_up.GetXaxis().SetMoreLogLabels()
hTotal_up.GetXaxis().SetRangeUser(100,3900)
hTotal_up.SetLineColor(kBlack)
hTotal_up.SetMarkerColor(kBlack)
hTotal_up.SetMarkerSize(0.5)
hTotal_up.GetXaxis().SetTitle("#it{p}_{T}")
hTotal_up.GetYaxis().SetTitle("Relative Uncertainty")
hTotal_down.Draw("same")
hTotal_down.SetMinimum(YMin)
hTotal_down.SetMaximum(YMax)
hTotal_down.GetXaxis().SetMoreLogLabels()
hTotal_down.GetXaxis().SetRangeUser(100,3900)
hTotal_down.SetLineColor(kBlack)
hTotal_down.SetMarkerColor(kBlack)
hTotal_down.SetMarkerSize(0.5)
hTotal_down.GetXaxis().SetTitle("#it{p}_{T}")
hTotal_down.GetYaxis().SetTitle("Relative Uncertainty")
legend.AddEntry(hTotal_up,"Total")
legend.Draw("same")
canvas.Print(OutPDF)
canvas.Print(OutPDF+"]")



InputFile.Close()
print ">>>> DONE <<<<"


