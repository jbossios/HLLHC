#!/usr/bin/python
import os, sys
from ROOT import *
from array import array
param=sys.argv

IJXS               = True # if False then DIJETS
MC15c              = True
HLLHC_optimistic   = True
HLLHC_conservative = True
HLLHC_pesimistic   = True

Freeze = True # FIXME FOR DIJETS
Smooth = False

####################################################################
gROOT.SetBatch(True)  # so does not pop up plots!

# ATLAS style
gROOT.LoadMacro("/home/jbossios/cern/AtlasStyle/AtlasStyle.C")
SetAtlasStyle()

# Load easyPlot
gROOT.LoadMacro("Plotter1D.cxx")
gROOT.LoadMacro("HelperClasses.cxx")
gROOT.LoadMacro("HelperFunctions.cxx")

# Plotter Instance for fomatting
Format = Plotter1D()   # This is the constructor for format only
Format.SetComparisonType("Normal")   # Will show second panel with ratio
Format.SetLogx()
Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
if IJXS:
  Format.SetAxisTitles("#it{p}_{T} [GeV]","Relative uncertainty") # x-axis, y-axis
else:
  Format.SetAxisTitles("#m_{jj} [GeV]","Relative uncertainty") # x-axis, y-axis
Format.SetLegendPosition("Top")  # Other options: Top, TopLeft, Bottom
#Format.SetRange("Y",-0.2,0.3)
#if IJXS:
#  Format.SetRange("Y",-0.14,0.37)
#else:
# x-axis range
if IJXS:
  Format.SetRange("X",100,3936)
else:
  Format.SetRange("X",100,7500)
# y-axis range
Format.SetRange("Y",-0.2,0.2)
if Freeze and IJXS:
  Format.SetRange("Y",-0.15,0.25)
if not Freeze and not IJXS:
  Format.SetRange("Y",-0.4,0.7)
Format.SetLineWidthAll(2)
Format.SetDrawOptionAll("HIST][")
Format.SetMoreLogLabelsX()
if Smooth:
  Format.SmoothAll(2)

systs_MC15c = [
  "JET_EffectiveNP_1__1up", # DONE
  "JET_EffectiveNP_1__1down", # DONE
  "JET_EffectiveNP_2__1up", # DONE
  "JET_EffectiveNP_2__1down", # DONE
  "JET_EffectiveNP_3__1up", # DONE
  "JET_EffectiveNP_3__1down", # DONE
  "JET_EffectiveNP_4__1up", # DONE
  "JET_EffectiveNP_4__1down", # DONE
  "JET_EffectiveNP_5__1up", # DONE
  "JET_EffectiveNP_5__1down", # DONE
  "JET_EffectiveNP_6__1up", # DONE
  "JET_EffectiveNP_6__1down", # DONE
  "JET_EffectiveNP_7__1up", # DONE
  "JET_EffectiveNP_7__1down", # DONE
  "JET_EffectiveNP_8restTerm__1up", # DONE
  "JET_EffectiveNP_8restTerm__1down", # DONE
  "JET_EtaIntercalibration_Modelling__1up", # DONE
  "JET_EtaIntercalibration_Modelling__1down", # DONE
  "JET_EtaIntercalibration_NonClosure__1up", # DONE
  "JET_EtaIntercalibration_NonClosure__1down", # DONE
  "JET_EtaIntercalibration_TotalStat__1up", # DONE
  "JET_EtaIntercalibration_TotalStat__1down", # DONE
  "JET_Flavor_Composition__1up", # DONE
  "JET_Flavor_Composition__1down", # DONE
  "JET_Flavor_Response__1up", # DONE
  "JET_Flavor_Response__1down", # DONE
  "JET_Pileup_OffsetMu__1up", # DONE
  "JET_Pileup_OffsetMu__1down", # DONE
  "JET_Pileup_OffsetNPV__1up", # DONE
  "JET_Pileup_OffsetNPV__1down", # DONE
  "JET_Pileup_PtTerm__1up", # DONE
  "JET_Pileup_PtTerm__1down", # DONE
  "JET_Pileup_RhoTopology__1up", # DONE
  "JET_Pileup_RhoTopology__1down", # DONE
  "JET_PunchThrough_MC15__1up", # DONE
  "JET_PunchThrough_MC15__1down", # DONE
  "JET_SingleParticle_HighPt__1up", # DONE
  "JET_SingleParticle_HighPt__1down", # DONE
  "JET_JER_SINGLE_NP__1up", # DONE 
]

systs_HLLHC_optimistic=[ # HLLHC optimistic
  "JET_EffectiveNP_1__1up", # DONE
  "JET_EffectiveNP_1__1down", # DONE
  "JET_EffectiveNP_2__1up",
  "JET_EffectiveNP_2__1down",
  "JET_EffectiveNP_3__1up",
  "JET_EffectiveNP_3__1down",
  "JET_EffectiveNP_4__1up",
  "JET_EffectiveNP_4__1down",
  "JET_EffectiveNP_5__1up",
  "JET_EffectiveNP_5__1down",
  "JET_EffectiveNP_6__1up",
  "JET_EffectiveNP_6__1down",
  "JET_EffectiveNP_7__1up",
  "JET_EffectiveNP_7__1down",
  "JET_EffectiveNP_8restTerm__1up",
  "JET_EffectiveNP_8restTerm__1down",
  "JET_Flavor_Response__1up",
  "JET_Flavor_Response__1down",
  "JET_Pileup_RhoTopology__1up",
  "JET_Pileup_RhoTopology__1down",
  "JET_JER_SINGLE_NP__1up",
]


systs_HLLHC_conservative=[ # HLLHC conservative
  "JET_EffectiveNP_1__1up",
  "JET_EffectiveNP_1__1down",
  "JET_EffectiveNP_2__1up",
  "JET_EffectiveNP_2__1down",
  "JET_EffectiveNP_3__1up",
  "JET_EffectiveNP_3__1down",
  "JET_EffectiveNP_4__1up",
  "JET_EffectiveNP_4__1down",
  "JET_EffectiveNP_5__1up",
  "JET_EffectiveNP_5__1down",
  "JET_EffectiveNP_6__1up",
  "JET_EffectiveNP_6__1down",
  "JET_EffectiveNP_7__1up",
  "JET_EffectiveNP_7__1down",
  "JET_EffectiveNP_8restTerm__1up",
  "JET_EffectiveNP_8restTerm__1down",
  "JET_EtaIntercalibration_Modelling__1up",
  "JET_EtaIntercalibration_Modelling__1down",
  "JET_Flavor_Response__1up",
  "JET_Flavor_Response__1down",
  "JET_Pileup_RhoTopology__1up",
  "JET_Pileup_RhoTopology__1down",
  "JET_JER_SINGLE_NP__1up",
]


####################################################################################
# DO NOT MODIFY
####################################################################################

# pT bins
npTavgBins = 46;
pTavgBins = [15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937., 4941., 6200., 7780., 9763.]

#npTavgBins_MC15c = 42
#pTavgBins_MC15c  = [15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500., 3137., 3937.]

npTavgBins_MC15c = 40
pTavgBins_MC15c  = [15., 20., 25., 35., 45., 55., 70., 85., 100., 116., 134., 152., 172., 194., 216., 240., 264., 290., 318., 346., 376., 408., 442., 478., 516., 556., 598., 642., 688., 736., 786., 838., 894., 952., 1012., 1076., 1162., 1310., 1530., 1992., 2500.]


hTotalUp_MC15c   = TH1D("MC15c_Total","",npTavgBins,array('d',pTavgBins))
hTotalDown_MC15c = TH1D("MC15c_Total_down","",npTavgBins,array('d',pTavgBins))
#hTotalUp_MC15c   = TH1D("MC15c_Total","",npTavgBins_MC15c,array('d',pTavgBins_MC15c))
#hTotalDown_MC15c = TH1D("MC15c_Total_down","",npTavgBins_MC15c,array('d',pTavgBins_MC15c))
hTotalUp_HLLHC_optimistic   = TH1D("HLLHC_optimistic_Total","",npTavgBins,array('d',pTavgBins))
hTotalDown_HLLHC_optimistic = TH1D("HLLHC_optimistic_Total_down","",npTavgBins,array('d',pTavgBins))
hTotalUp_HLLHC_conservative   = TH1D("HLLHC_conservative_Total","",npTavgBins,array('d',pTavgBins))
hTotalDown_HLLHC_conservative = TH1D("HLLHC_conservative_Total_down","",npTavgBins,array('d',pTavgBins))
hTotalUp_HLLHC_pesimistic       = TH1D("HLLHC_pesimistic_Total","",npTavgBins,array('d',pTavgBins))
hTotalDown_HLLHC_pesimistic     = TH1D("HLLHC_pesimistic_Total_down","",npTavgBins,array('d',pTavgBins))

OutPDF = "../Plots/Compare_MC15_vs_HLLHC"
if IJXS:
  OutPDF += "_IJXS"
else:
  OutPDF += "_DIJETS"

if MC15c:
  InputFileName  = "../Inputs/"
  if IJXS:
    InputFileName += "IJXS_"
  else:
    InputFileName += "DIJETS_"
  InputFileName += "MC15c.root"
  InputFile = TFile.Open(InputFileName)
  if not InputFile:
    print "InputFile not found, exiting"
    sys.exit(0)
  Systs = systs_MC15c
  # Total up
  LastUncertainty = 0
  for bin in range(1,npTavgBins+1):
    if bin < 41 or not Freeze:
      TotalUncertainty = 0;
      for syst in Systs:
        if "1up" in syst:
          VarHist = InputFile.Get(syst)
          if not VarHist:
            print "VarHist not found, exiting"
            sys.exit(0)
          Uncertainty = VarHist.GetBinContent(bin)
          #Error       = VarHist.GetBinError(bin)
          #if Uncertainty < uncMax and Uncertainty > -uncMax:
          #if abs(Error) < 0.2 and Uncertainty < uncMax and Uncertainty > -uncMax:
          TotalUncertainty += Uncertainty * Uncertainty
      TotalUncertainty = TMath.Sqrt(TotalUncertainty)
      if bin == 40:
        LastUncertainty = TotalUncertainty
      hTotalUp_MC15c.SetBinContent(bin,TotalUncertainty)
      hTotalUp_MC15c.SetBinError(bin,0)
  if Freeze:
    hTotalUp_MC15c.SetBinContent(41,TotalUncertainty)
    hTotalUp_MC15c.SetBinContent(42,TotalUncertainty)
    hTotalUp_MC15c.SetBinError(41,0)
    hTotalUp_MC15c.SetBinError(42,0)
  # Total down
  LastUncertainty = 0
  for bin in range(1,npTavgBins+1):
    if bin < 41 or not Freeze:
      TotalUncertainty = 0;
      for syst in Systs:
        if "1down" in syst:
          VarHist = InputFile.Get(syst)
          if not VarHist:
            print "VarHist not found, exiting"
            sys.exit(0)
          Uncertainty = VarHist.GetBinContent(bin)
          TotalUncertainty += Uncertainty * Uncertainty
        if "SINGLE" in syst:
          VarHist = InputFile.Get(syst)
          if not VarHist:
            print "VarHist not found, exiting"
            sys.exit(0)
          Uncertainty = VarHist.GetBinContent(bin)
          #Error       = VarHist.GetBinError(bin)
          #if Uncertainty < uncMax and Uncertainty > -uncMax:
          #if abs(Error) < 0.2 and Uncertainty < uncMax and Uncertainty > -uncMax:
          TotalUncertainty += Uncertainty * Uncertainty
      TotalUncertainty = TMath.Sqrt(TotalUncertainty)
      if bin == 40:
        LastUncertainty = TotalUncertainty
      hTotalDown_MC15c.SetBinContent(bin,TotalUncertainty)
      hTotalDown_MC15c.SetBinError(bin,0)
  if Freeze:
    hTotalDown_MC15c.SetBinContent(41,LastUncertainty)
    hTotalDown_MC15c.SetBinContent(42,LastUncertainty)
    hTotalDown_MC15c.SetBinError(41,0)
    hTotalDown_MC15c.SetBinError(42,0)
  hTotalDown_MC15c.Scale(-1)

if HLLHC_optimistic:
  InputFileName  = "../Inputs/"
  if IJXS:
    InputFileName += "IJXS_"
  else:
    InputFileName += "DIJETS_"
  InputFileName += "HLLHC_optimistic.root"
  InputFile = TFile.Open(InputFileName)
  if not InputFile:
    print "InputFile not found, exiting"
    sys.exit(0)
  Systs = systs_HLLHC_optimistic
  # Total up
  LastUncertainty = 0
  for bin in range(1,npTavgBins+1):
    if bin < 41 or not Freeze:
      TotalUncertainty = 0;
      for syst in Systs:
        if "1up" in syst:
          VarHist = InputFile.Get(syst)
          if not VarHist:
            print "VarHist not found, exiting"
            sys.exit(0)
          Uncertainty = VarHist.GetBinContent(bin)
          TotalUncertainty += Uncertainty * Uncertainty
      TotalUncertainty = TMath.Sqrt(TotalUncertainty)
      if bin == 40:
        LastUncertainty = TotalUncertainty
      hTotalUp_HLLHC_optimistic.SetBinContent(bin,TotalUncertainty)
      hTotalUp_HLLHC_optimistic.SetBinError(bin,0)
  if Freeze:
    hTotalUp_HLLHC_optimistic.SetBinContent(41,LastUncertainty)
    hTotalUp_HLLHC_optimistic.SetBinContent(42,LastUncertainty)
    hTotalUp_HLLHC_optimistic.SetBinError(41,0)
    hTotalUp_HLLHC_optimistic.SetBinError(42,0)
    
  # Total down
  LastUncertainty
  for bin in range(1,npTavgBins+1):
    if bin < 41 or not Freeze:
      TotalUncertainty = 0;
      for syst in Systs:
        if "1down" in syst:
          VarHist = InputFile.Get(syst)
          if not VarHist:
            print "VarHist not found, exiting"
            sys.exit(0)
          Uncertainty = VarHist.GetBinContent(bin)
          TotalUncertainty += Uncertainty * Uncertainty
        if "SINGLE" in syst:
          VarHist = InputFile.Get(syst)
          if not VarHist:
            print "VarHist not found, exiting"
            sys.exit(0)
          Uncertainty = VarHist.GetBinContent(bin)
          TotalUncertainty += Uncertainty * Uncertainty
      TotalUncertainty = TMath.Sqrt(TotalUncertainty)
      if bin == 40:
        LastUncertainty = TotalUncertainty
      hTotalDown_HLLHC_optimistic.SetBinContent(bin,TotalUncertainty)
      hTotalDown_HLLHC_optimistic.SetBinError(bin,0)
  if Freeze:
    hTotalDown_HLLHC_optimistic.SetBinContent(41,LastUncertainty)
    hTotalDown_HLLHC_optimistic.SetBinContent(42,LastUncertainty)
    hTotalDown_HLLHC_optimistic.SetBinError(41,0)
    hTotalDown_HLLHC_optimistic.SetBinError(42,0)
  hTotalDown_HLLHC_optimistic.Scale(-1)

if HLLHC_conservative:
  InputFileName  = "../Inputs/"
  if IJXS:
    InputFileName += "IJXS_"
  else:
    InputFileName += "DIJETS_"
  InputFileName += "HLLHC_conservative.root"
  InputFile = TFile.Open(InputFileName)
  if not InputFile:
    print "InputFile not found, exiting"
    sys.exit(0)
  Systs = systs_HLLHC_conservative
  # Total up
  LastUncertainty = 0
  for bin in range(1,npTavgBins+1):
    if bin < 41 or not Freeze:
      TotalUncertainty = 0;
      for syst in Systs:
        if "1up" in syst:
          VarHist = InputFile.Get(syst)
          if not VarHist:
            print "VarHist not found, exiting"
            sys.exit(0)
          Uncertainty = VarHist.GetBinContent(bin)
          TotalUncertainty += Uncertainty * Uncertainty
      TotalUncertainty = TMath.Sqrt(TotalUncertainty)
      if bin == 40:
        LastUncertainty = TotalUncertainty
      hTotalUp_HLLHC_conservative.SetBinContent(bin,TotalUncertainty)
      hTotalUp_HLLHC_conservative.SetBinError(bin,0)
  if Freeze:
    hTotalUp_HLLHC_conservative.SetBinContent(41,LastUncertainty)
    hTotalUp_HLLHC_conservative.SetBinContent(42,LastUncertainty)
    hTotalUp_HLLHC_conservative.SetBinError(41,0)
    hTotalUp_HLLHC_conservative.SetBinError(42,0)
  # Total down
  LastUncertainty = 0
  for bin in range(1,npTavgBins+1):
    if bin < 41 or not Freeze:
      TotalUncertainty = 0;
      for syst in Systs:
        if "1down" in syst:
          VarHist = InputFile.Get(syst)
          if not VarHist:
            print "VarHist not found, exiting"
            sys.exit(0)
          Uncertainty = VarHist.GetBinContent(bin)
          TotalUncertainty += Uncertainty * Uncertainty
        if "SINGLE" in syst:
          VarHist = InputFile.Get(syst)
          if not VarHist:
            print "VarHist not found, exiting"
            sys.exit(0)
          Uncertainty = VarHist.GetBinContent(bin)
          TotalUncertainty += Uncertainty * Uncertainty
      TotalUncertainty = TMath.Sqrt(TotalUncertainty)
      if bin == 40:
        LastUncertainty = TotalUncertainty
      hTotalDown_HLLHC_conservative.SetBinContent(bin,TotalUncertainty)
      hTotalDown_HLLHC_conservative.SetBinError(bin,0)
  if Freeze:
    hTotalDown_HLLHC_conservative.SetBinContent(41,LastUncertainty)
    hTotalDown_HLLHC_conservative.SetBinContent(42,LastUncertainty)
    hTotalDown_HLLHC_conservative.SetBinError(41,0)
    hTotalDown_HLLHC_conservative.SetBinError(42,0)
  hTotalDown_HLLHC_conservative.Scale(-1)


if HLLHC_pesimistic:
  InputFileName  = "../Inputs/"
  if IJXS:
    InputFileName += "IJXS_"
  else:
    InputFileName += "DIJETS_"
  InputFileName += "HLLHC_pesimistic.root"
  InputFile = TFile.Open(InputFileName)
  if not InputFile:
    print "InputFile not found, exiting"
    sys.exit(0)
  Systs = systs_HLLHC_conservative
  # Total up
  LastUncertainty = 0
  for bin in range(1,npTavgBins+1):
    if bin < 41 or not Freeze:
      TotalUncertainty = 0;
      for syst in Systs:
        if "1up" in syst:
          VarHist = InputFile.Get(syst)
          if not VarHist:
            print "VarHist not found, exiting"
            sys.exit(0)
          Uncertainty = VarHist.GetBinContent(bin)
          TotalUncertainty += Uncertainty * Uncertainty
      TotalUncertainty = TMath.Sqrt(TotalUncertainty)
      if bin == 40:
        LastUncertainty = TotalUncertainty
      hTotalUp_HLLHC_pesimistic.SetBinContent(bin,TotalUncertainty)
      hTotalUp_HLLHC_pesimistic.SetBinError(bin,0)
  if Freeze:
    hTotalUp_HLLHC_pesimistic.SetBinContent(41,LastUncertainty)
    hTotalUp_HLLHC_pesimistic.SetBinContent(42,LastUncertainty)
    hTotalUp_HLLHC_pesimistic.SetBinError(41,0)
    hTotalUp_HLLHC_pesimistic.SetBinError(42,0)
  # Total down
  LastUncertainty = 0
  for bin in range(1,npTavgBins+1):
    if bin < 41 or not Freeze:
      TotalUncertainty = 0;
      for syst in Systs:
        if "1down" in syst:
          VarHist = InputFile.Get(syst)
          if not VarHist:
            print "VarHist not found, exiting"
            sys.exit(0)
          Uncertainty = VarHist.GetBinContent(bin)
          TotalUncertainty += Uncertainty * Uncertainty
        if "SINGLE" in syst:
          VarHist = InputFile.Get(syst)
          if not VarHist:
            print "VarHist not found, exiting"
            sys.exit(0)
          Uncertainty = VarHist.GetBinContent(bin)
          TotalUncertainty += Uncertainty * Uncertainty
      TotalUncertainty = TMath.Sqrt(TotalUncertainty)
      if bin == 40:
        LastUncertainty = TotalUncertainty
      hTotalDown_HLLHC_pesimistic.SetBinContent(bin,TotalUncertainty)
      hTotalDown_HLLHC_pesimistic.SetBinError(bin,0)
  if Freeze:
    hTotalDown_HLLHC_pesimistic.SetBinContent(41,LastUncertainty)
    hTotalDown_HLLHC_pesimistic.SetBinContent(42,LastUncertainty)
    hTotalDown_HLLHC_pesimistic.SetBinError(41,0)
    hTotalDown_HLLHC_pesimistic.SetBinError(42,0)
  hTotalDown_HLLHC_pesimistic.Scale(-1)

# Final Plot
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
if MC15c:
  Plot.AddHist("MC15cUP",hTotalUp_MC15c)
  Plot.SetLegend("MC15cUP","MC15c")
  Plot.SetColor("MC15cUP",kBlack)
  Plot.AddHist("MC15cDOWN",hTotalDown_MC15c)
  Plot.SetColor("MC15cDOWN",kBlack)
  Plot.NoTLegend("MC15cDOWN")
  Plot.SetLineStyle("MC15cUP",1)
  Plot.SetLineStyle("MC15cDOWN",1)
if HLLHC_pesimistic:
  Plot.AddHist("HLLHC_pesimisticUP",hTotalUp_HLLHC_pesimistic)
  Plot.SetLegend("HLLHC_pesimisticUP","HLLHC pesimistic")
  Plot.SetColor("HLLHC_pesimisticUP",kGreen+2)
  Plot.AddHist("HLLHC_pesimisticDOWN",hTotalDown_HLLHC_pesimistic)
  Plot.SetColor("HLLHC_pesimisticDOWN",kGreen+2)
  Plot.NoTLegend("HLLHC_pesimisticDOWN")
  Plot.SetLineStyle("HLLHC_pesimisticUP",4)
  Plot.SetLineStyle("HLLHC_pesimisticDOWN",4)
if HLLHC_conservative:
  Plot.AddHist("HLLHC_conservativeUP",hTotalUp_HLLHC_conservative)
  Plot.SetLegend("HLLHC_conservativeUP","HLLHC conservative")
  Plot.SetColor("HLLHC_conservativeUP",kRed+2)
  Plot.AddHist("HLLHC_conservativeDOWN",hTotalDown_HLLHC_conservative)
  Plot.SetColor("HLLHC_conservativeDOWN",kRed+2)
  Plot.NoTLegend("HLLHC_conservativeDOWN")
  Plot.SetLineStyle("HLLHC_conservativeUP",2)
  Plot.SetLineStyle("HLLHC_conservativeDOWN",2)
if HLLHC_optimistic:
  Plot.AddHist("HLLHC_optimisticUP",hTotalUp_HLLHC_optimistic)
  Plot.SetLegend("HLLHC_optimisticUP","HLLHC optimistic")
  Plot.SetColor("HLLHC_optimisticUP",kBlue+2)
  Plot.AddHist("HLLHC_optimisticDOWN",hTotalDown_HLLHC_optimistic)
  Plot.SetColor("HLLHC_optimisticDOWN",kBlue+2)
  Plot.NoTLegend("HLLHC_optimisticDOWN")
  Plot.SetLineStyle("HLLHC_optimisticUP",3)
  Plot.SetLineStyle("HLLHC_optimisticDOWN",3)
Plot.Write()  # Saves plot into OutPDF

print ">>>> DONE <<<<"


