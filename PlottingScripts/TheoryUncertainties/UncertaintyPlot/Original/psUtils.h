
void divideBinWidth(TH1D*& h )
{
  Float_t nEnt = h->GetEntries();
  for (int iBin=1; iBin<=h->GetNbinsX();iBin++)
    {
      double binwidth = h->GetBinWidth(iBin);
      double bincontent = h->GetBinContent(iBin);
      double bincontenterror = h->GetBinError(iBin);
      if ( binwidth > 0 )
	{
	  h->SetBinContent( iBin,bincontent/binwidth);
	  h->SetBinError(iBin,bincontenterror/binwidth );
	}
    }
  h->SetEntries( nEnt );
  return;
}

void multiplyBinWidth(TH1D*& h )
{
  Float_t nEnt = h->GetEntries();
  for (int iBin=1; iBin<=h->GetNbinsX();iBin++)
    {
      double binwidth = h->GetBinWidth(iBin);
      double bincontent = h->GetBinContent(iBin);
      double bincontenterror = h->GetBinError(iBin);
      if ( binwidth > 0 )
	{
	  h->SetBinContent( iBin,bincontent*binwidth);
	  h->SetBinError(iBin,bincontenterror*binwidth );
	}
    }
  h->SetEntries( nEnt );
  return;
}

TGraph* psMakeBand(THStack* inputStack, TString option = "quadrature")
{
  //
  //  makes graph with errors out of stack of histograms
  //

  int debug = 1;
  if (!inputStack)
    {
      cout <<"histogram stack is not found "<< endl;
      return 0;
    }
  
  TList*  localList = inputStack->GetHists();
  if (!localList)
    {
      //cout <<"histogram stack is empty "<< endl;
      return 0;
    }    

  const Int_t listSize(localList->GetSize());
  
  Int_t nPoints = ((TH1D*)(localList->At(0)))->GetNbinsX();
  
  TGraphErrors* centralValue = (TGraphErrors*)psTH12TGraph((TH1D*)localList->At(0));
  
  TGraphAsymmErrors* graphBand = new TGraphAsymmErrors( nPoints );
  graphBand->SetPoint(0,0,0);
  graphBand->SetPointError(0,0,0,0,0);

   //std::cout << std::flush; 
   //std::cout <<" \n\n "<<option<<"listSize = "<<listSize<<" nPoints = "<<nPoints<<" cv Npoints = "<<centralValue->GetN()<< std::endl; 
   //cout << option << endl;
   //std::cout << std::flush; 


  Int_t switchCase = 0;
  Double_t clFactor = 1.64485;
  Double_t bandFactor = 1.;
  option.ToLower();

  if ( option.Contains("abkm") || option.Contains("abm") || option.Contains("alek") ) 
    {
      switchCase = 50;
      bandFactor = clFactor;
    }
  if (option.Contains("gjr") || option.Contains("jr")) 
    {
      switchCase = 10;
      bandFactor = clFactor;
    }
  if (option.Contains("nnpdf") || option.Contains("meta")) 
    {
      switchCase = 100;
      bandFactor = clFactor;
    }
  if (option.Contains("hera") && !option.Contains("var")) 
    {
      switchCase = 10;
      bandFactor = clFactor;
    }
  if (option.Contains("hera") && option.Contains("var")) 
    {
      std::cout << "\n\n HERA VAR set has been detected "<< option << "\n\n"<< std::endl;
	if (option.Contains("15"))
        switchCase = 10115;
        else if (option.Contains("20"))
        switchCase = 10120;
        else
        {
        switchCase = -111;
         std::cout << "\n\n Wrong HERAPDF version : " << option << std::endl;
         }
      bandFactor = 1.;
    }
  if (option.Contains("envelope")) switchCase = 1111;
  if (option.Contains("cteq") || option.Contains("ct10")|| option.Contains("ct14")) switchCase = 10;
  if (option.Contains("mrst") || option.Contains("mstw")|| option.Contains("mmht"))
    {
      switchCase = 10;
      bandFactor = clFactor;
    }
  if (option.Contains("alphas")) 
    {
      switchCase = 10;
      bandFactor = 12./10.;
      if (option.Contains("02")) bandFactor=12./20.;
      if (option.Contains("03")) bandFactor=12./30.;
    }
  if (option.Contains("quadrature")) switchCase = 50;

   //cout << option <<" sc "<< switchCase<<" bf "<<bandFactor<< endl;
   //std::cout << "\n final \n\n "<< std::flush; 

  
  for (int iPoint = 1; iPoint <= nPoints; iPoint++)
    {
      Double_t otherYValues[5000];
      Double_t centralXValue, centralXValueError, centralYValue, centralYValueErrorL, centralYValueErrorH;
      centralValue->GetPoint(iPoint, centralXValue, centralYValue);
      centralXValueError = 0.5*centralValue->GetErrorX(iPoint);

      graphBand->SetPoint(iPoint, centralXValue, centralYValue);
      graphBand->SetPointEXhigh(iPoint, centralXValueError);
      graphBand->SetPointEXlow (iPoint, centralXValueError);

      for (int iHist = 1; iHist < listSize; iHist++)
	{
	  TH1D* htemp = (TH1D*)localList->At(iHist);
	  //htemp->Print();
	  otherYValues[iHist] = htemp->GetBinContent(htemp->FindBin(centralXValue));
	}

      switch (switchCase)
	{
	case (1111):
	  calculateErrorsEnvelope(listSize, otherYValues, 
				  centralYValue, 
				  centralYValueErrorL, 
				  centralYValueErrorH);
	    //cout<<"L = "<<centralYValueErrorL<<" H = "<<centralYValueErrorH<<endl;
	  graphBand->SetPointEYhigh(iPoint, centralYValueErrorH);
	  graphBand->SetPointEYlow (iPoint, centralYValueErrorL);
	  break;

	case (100):
	      calculateErrorsNNPDF(listSize, otherYValues, centralYValue, centralYValueErrorH);
	      graphBand->SetPointEYhigh(iPoint, bandFactor * centralYValueErrorH);
	      graphBand->SetPointEYlow (iPoint, bandFactor * centralYValueErrorH);
	  break;
	case (10):
	  if (option.Contains("asym"))
	    {
	      calculateErrorsHessianAsymmetric(listSize, otherYValues, 
					       centralYValue, 
					       centralYValueErrorL, 
					       centralYValueErrorH);
	      graphBand->SetPointEYhigh(iPoint, bandFactor * centralYValueErrorH);
	      graphBand->SetPointEYlow (iPoint, bandFactor * centralYValueErrorL);
	    }
	  else if (option.Contains("alphas"))
	    {
	      calculateErrorsHessianAsymmetric(listSize, otherYValues,
					       centralYValue, 
					       centralYValueErrorL, 
					       centralYValueErrorH);
     	      graphBand->SetPointEYhigh(iPoint, bandFactor * centralYValueErrorH);
	      graphBand->SetPointEYlow (iPoint, bandFactor * centralYValueErrorL);
	    }
	  else
	    {
	      calculateErrorsHessianSymmetric(listSize, otherYValues, centralYValueErrorH);
	      graphBand->SetPointEYhigh(iPoint, bandFactor * centralYValueErrorH);
	      graphBand->SetPointEYlow (iPoint, bandFactor * centralYValueErrorH);
	    }
	  break;
	  // 	case (20):
	  // 	  break;
	  
	  // 	case (30):
	  // 	  break;
	case (50):
	  calculateErrorsSymmetric(listSize, otherYValues, centralYValue, centralYValueErrorH);
	  graphBand->SetPointEYhigh(iPoint, bandFactor * centralYValueErrorH);
	  graphBand->SetPointEYlow (iPoint, bandFactor * centralYValueErrorH);
	  break;

	case (10115): //HERAVAR 15
	  calculateErrorsHERAVAR(15,listSize, otherYValues, centralYValue, centralYValueErrorL, centralYValueErrorH);
	  graphBand->SetPointEYhigh(iPoint, bandFactor * centralYValueErrorH);
	  graphBand->SetPointEYlow (iPoint, bandFactor * centralYValueErrorL);
          break;
	case (10120): //HERAVAR 20
	  calculateErrorsHERAVAR(20,listSize, otherYValues, centralYValue, centralYValueErrorL, centralYValueErrorH);
	  graphBand->SetPointEYhigh(iPoint, bandFactor * centralYValueErrorH);
	  graphBand->SetPointEYlow (iPoint, bandFactor * centralYValueErrorL);
	  break;
	default:
	  //calculateErrorsSymmetric(listSize, otherYValues, centralYValue, centralYValueErrorH);
	  centralYValueErrorH = 2. *centralYValue;  // just to test
	  graphBand->SetPointEYhigh(iPoint, centralYValueErrorH);
	  graphBand->SetPointEYlow (iPoint, centralYValueErrorH);
	  break;
	}

    }
 
  // if (option.Contains("envelope")) graphBand->Print();
  //  cout<<"\tmax = "<<graphBand->GetMaximum()<<"\t";
  //  cout<<"\tmin = "<<graphBand->GetMinimum()<<endl<<endl;

  return graphBand;
}

void  calculateErrorsEnvelope(const Int_t nValues, const Double_t *oValues, Double_t cValue, Double_t &cValueErrorL, Double_t &cValueErrorH)
{
  
  cValueErrorL = 0.;
  cValueErrorH = 0.;
  
  for (int i = 1; i < nValues ; i++)
    {
      //cout <<"i = "<<i<<" cValue = "<<cValue<<" var = "<<oValues[i]<< endl;
      Double_t diff = oValues[i] - cValue;
      if (diff >= 0.)
  	  cValueErrorH = TMath::Max(cValueErrorH, diff);
      else
  	  cValueErrorL = TMath::Max(cValueErrorL, -diff);
    }
  //cout <<"errorL = "<<cValueErrorL<<" errorH = "<<cValueErrorH<< endl;
  return;
}
void  calculateErrorsHessianAsymmetric(const Int_t nValues, Double_t *oValues, Double_t cValue, Double_t &cValueErrorL, Double_t &cValueErrorH)
{

  cValueErrorL = 0.;
  cValueErrorH = 0.;

  for (int i = 1; i < nValues ; i += 2)
    {
      Double_t diffPlus = oValues[i] - cValue;
      Double_t diffMinus = oValues[i+1] - cValue;

      Double_t currentErrorPlus = 0.;
      currentErrorPlus = TMath::Max(currentErrorPlus, diffPlus);
      currentErrorPlus = TMath::Max(currentErrorPlus, diffMinus);

      Double_t currentErrorMinus = 0.;
      currentErrorMinus = TMath::Max(currentErrorMinus, -diffPlus);
      currentErrorMinus = TMath::Max(currentErrorMinus, -diffMinus);

      cValueErrorL += currentErrorMinus * currentErrorMinus;
      cValueErrorH += currentErrorPlus * currentErrorPlus;
    }

  cValueErrorL = TMath::Sqrt(cValueErrorL);
  cValueErrorH = TMath::Sqrt(cValueErrorH);
}

void  calculateErrorsHessianSymmetric(Int_t nValues, Double_t *oValues, Double_t &cValueError)
{

  cValueError = 0.;

  for (int i = 1; i < nValues ; i += 2)
    {
      Double_t diff = oValues[i] - oValues[i+1];

      cValueError += diff * diff;
    }

  cValueError = 0.5 * TMath::Sqrt(cValueError);
}


void calculateErrorsSymmetric(Int_t nValues, Double_t *oValues, Double_t cValue, Double_t &cValueError)
{
  cValueError = 0.;

  for (int i = 1; i < nValues ; i++)
    {
      Double_t diff = cValue - oValues[i];
      cValueError += diff*diff;
    }

  cValueError = std::sqrt(cValueError);
}

void calculateErrorsAsymmetric(Int_t nValues, Double_t *oValues, Double_t cValue, Double_t &cValueErrorL, Double_t &cValueErrorH)
{
  cValueErrorL = 0.;
  cValueErrorH = 0.;

  for (int i = 1; i < nValues ; i++)
    {
      Double_t diff = cValue - oValues[i];
      if (diff >= 0.)
	{
	  cValueErrorH += diff*diff;
	}
      else
	{
	  cValueErrorL += diff*diff;
	}
    }

  cValueErrorL = TMath::Sqrt(cValueErrorL);
  cValueErrorH = TMath::Sqrt(cValueErrorH);
}

void calculateErrorsNNPDF(Int_t nValues, Double_t *oValues, Double_t cValue, Double_t &cValueError)
{
  cValueError = 0.;
  for (int i = 1; i < nValues ; i++)
    {
      Double_t diff = cValue - oValues[i];
      cValueError += diff*diff;
    }

  cValueError = TMath::Sqrt(cValueError/(nValues - 2.));

}


void calculateErrorsHERAVAR(Int_t version, Int_t nValues, Double_t *oValues, Double_t cValue, Double_t &cValueErrorL, Double_t &cValueErrorH)
{
//  HERE the HERA PDF 2.0 is assumed. 
//  First 1-10 are added in quadrature pos and neg separately
//  Second 11-14 the largest deviation from the central value is taken as the uncert. also separately for pos. and neg.
//
Int_t nMax(13), nModel(11);
if (version == 15) {nMax=12;nModel=9;}
if((version!=20) && (version!=15)) std::cout <<"wrong HERAVAR version (calculateErrorsHERAVAR) "<<version<< std::endl;
if ( (nValues-1) != nMax) 
{
std::cout <<"Wrong number of variations : "<<nValues<<" ("<<nMax<<")"<< std::endl;
return;
} 

  cValueErrorL = 0.;
  cValueErrorH = 0.;

  for (int i = 1; i < nModel ; i++)
    {
      Double_t diff = cValue - oValues[i];
      if (diff >= 0.)
	{
	  cValueErrorH += diff*diff;
	}
      else
	{
	  cValueErrorL += diff*diff;
	}
    }

  Double_t  cValueErrorML = 0., cValueErrorMH = 0.;

  for (int i = nModel; i <= nMax ; i++)
    {
      Double_t diff = cValue - oValues[i];
      if (diff >= 0.)
	{
	  if (TMath::Abs(diff)>cValueErrorMH) cValueErrorMH = TMath::Abs(diff);
	}
      else
	{
	  if (TMath::Abs(diff)>cValueErrorML) cValueErrorML = TMath::Abs(diff);
	}
    }

  cValueErrorL = TMath::Sqrt( cValueErrorL + cValueErrorMH * cValueErrorMH );
  cValueErrorH = TMath::Sqrt( cValueErrorH + cValueErrorML * cValueErrorML );



}

TGraphAsymmErrors* psTGraph2TGraphAsymmErrors(TGraph* gr)
{
  
  Int_t nPoints = gr->GetN();
  TGraphAsymmErrors* g = new TGraphAsymmErrors(nPoints);
  //  std::cout <<" psTGraph2TGraphAsymmErrors "<<nPoints<<std::endl;

  for (Int_t iPoint = 0; iPoint < nPoints; iPoint++)
    {
      g->GetX()[iPoint] = gr->GetX()[iPoint];
      g->GetEXhigh()[iPoint] = 0.5*gr->GetEX()[iPoint];
      g->GetEXlow()[iPoint] = 0.5*gr->GetEX()[iPoint];

      g->GetY()[iPoint] = gr->GetY()[iPoint];
      g->GetEYhigh()[iPoint] = 0.5*gr->GetEY()[iPoint];
      g->GetEYlow()[iPoint] = 0.5*gr->GetEY()[iPoint];

      //  std::cout <<" psTGraph2TGraphAsymmErrors \t"<<iPoint<<std::endl;
    }

  return g;
}

TGraph* psTH12TGraph(TH1 *h1, bool binError = false)
{
  //
  // convert the histogram h1 into a graph
  //
  if (!h1) 
    {
      cout <<name<< " histogram not found !" << endl;
      return 0;
    }
  
  TGraphErrors* g1 = new TGraphErrors();


  Double_t x, y, ex, ey;
  for (Int_t i = 1; i <= h1->GetNbinsX(); i++) 
    {
      x =  h1->GetBinCenter(i);
      ex = h1->GetBinWidth(i);
      //--
      y = h1->GetBinContent(i);
      (binError) ? ey = h1->GetBinError(i): ey = 0;
      //--
      g1->SetPoint(i,x,y);
      g1->SetPointError(i,ex,ey);
    }

  g1->SetPoint(0,0,0);
  g1->SetPointError(0,0,0);
  g1->SetName( TString::Format("%s%s",h1->GetName(),"Graph"));
  g1->SetTitle( TString::Format("%s%s",h1->GetTitle(),""));

/*   Info("psTH12TGraph", h1->GetName()); */
/*   g1->Print("all"); */
/*   Info("psTH12TGraph","Done"); */
/*   std::cout << std::flush; */
  return g1;
}
//
TGraphAsymmErrors* psAddBands(TGraphAsymmErrors* g1, TGraphAsymmErrors* g2)
{
  TGraphAsymmErrors* totalBand = new TGraphAsymmErrors();
  if (g1->GetN() != g2->GetN())
    {
      cout<<"!!! ERROR in addBands(gr*, gr*)"<<endl;
      cout <<" number of points in graph #1 "<<g1->GetN()<<"is not equal to nember of points in graph #2 "<<g2->GetN()<<endl;
      return totalBand;
    }
  
  for (int iPoint = 1; iPoint < g1->GetN(); iPoint++)
    {
      Double_t xValue, yValue;
      Double_t xValueErrorL, xValueErrorH;
      Double_t yValueErrorL, yValueErrorH, yValueErrorL1, yValueErrorH1, yValueErrorL2, yValueErrorH2;
      
      g1->GetPoint(iPoint, xValue, yValue);
      xValueErrorL = g1->GetErrorXlow(iPoint);
      xValueErrorH = g1->GetErrorXhigh(iPoint);
      
      yValueErrorL1 = g1->GetErrorYlow(iPoint);
      yValueErrorH1 = g1->GetErrorYhigh(iPoint);
      yValueErrorL2 = g2->GetErrorYlow(iPoint);
      yValueErrorH2 = g2->GetErrorYhigh(iPoint);
      
      yValueErrorL = TMath::Sqrt(yValueErrorL1 * yValueErrorL1 + yValueErrorL2 * yValueErrorL2);
      yValueErrorH = TMath::Sqrt(yValueErrorH1 * yValueErrorH1 + yValueErrorH2 * yValueErrorH2);
      
      totalBand->SetPoint(iPoint, xValue, yValue);
      totalBand->SetPointEXhigh(iPoint, xValueErrorH);
      totalBand->SetPointEXlow (iPoint, xValueErrorL);
      totalBand->SetPointEYhigh(iPoint, yValueErrorH);
      totalBand->SetPointEYlow (iPoint, yValueErrorL);
    }

  return totalBand;
}
  //
void getBandRange(TGraphAsymmErrors* g, Double_t &MIN, Double_t &MAX)
{
  MIN = 1000.0;
  MAX = -1000.0;
  for (Int_t iPoint = 1; iPoint < g->GetN(); iPoint++)
    {
      Double_t xValue, yValue;
      Double_t yValueErrorHigh, yValueErrorLow;
      g->GetPoint(iPoint, xValue, yValue);
      yValueErrorHigh = g->GetErrorYhigh(iPoint);
      yValueErrorLow  = g->GetErrorYlow (iPoint);
      MIN = TMath::Min(MIN, yValue - yValueErrorLow);
      MAX = TMath::Max(MAX, yValue + yValueErrorHigh);
    }

}

//
//
//

//-----------------------------------------------
TGraphAsymmErrors* getBandsRatio(TGraphAsymmErrors* num, TGraphAsymmErrors* den, Int_t mOption = 0 )
//
//
//  option 0 : errors of result are the errors of num scaled by the value of den
//  option 1 : errors are divided as well
//
//
{
  //  Info("getBandsRatio"," in there ");

  //   num->Print();
  //    den->Print();
  
  TGraphAsymmErrors* graph_my = new TGraphAsymmErrors( *num );
  
  //  for (Int_t iBin = 1; iBin < 5; iBin++)
  for (Int_t iBin = 0; iBin <= num->GetN(); iBin++)
    {
      Double_t xValue = 0, yValue = 0;
      Double_t yErrorLow = 0., yErrorHigh = 0.;

      num->GetPoint(iBin, xValue, yValue);
      yErrorLow  = num->GetErrorYlow (iBin);
      yErrorHigh = num->GetErrorYhigh(iBin);


      Double_t yValueDivisor = 0;
      Double_t yErrorDenLow = 0., yErrorDenHigh = 0.;
      
      den->GetPoint(iBin, xValue, yValueDivisor);
      yErrorDenLow  = den->GetErrorYlow (iBin);
      yErrorDenHigh = den->GetErrorYhigh(iBin);
      
      
      if (0. != yValueDivisor)
	{
	  if ( -1 == mOption )
	    {
	      yErrorLow =  0.;
	      yErrorHigh = 0.;
	    }
	  if ( 0 == mOption )
	    {
	      yErrorLow  /= yValueDivisor;
	      yErrorHigh /= yValueDivisor;
	    }
	  if ( 1 == mOption )
	    {
	      //yErrorLow  = 1./yValueDivisor * TMath::Abs(yErrorLow  - yErrorDenLow * yValue/yValueDivisor );// /yValueDivisor;
	      //yErrorHigh = 1./yValueDivisor * TMath::Abs(yErrorHigh - yErrorDenHigh * yValue/yValueDivisor);///yValueDivisor;
	      yErrorLow  = ( yValue - yErrorLow ) /( yValueDivisor - yErrorDenLow  );
	      yErrorHigh = ( yValue + yErrorHigh )/( yValueDivisor + yErrorDenHigh );
	      yErrorLow  = TMath::Abs( yValue/yValueDivisor - yErrorLow  );
	      yErrorHigh = TMath::Abs( yValue/yValueDivisor - yErrorHigh );
	    }
	}
      
      //cout<< " opt = "<<mOption<<"\n low "<<yErrorLow<<" high = "<<yErrorLow<< endl;
      graph_my->SetPoint(iBin, xValue, yValue/yValueDivisor);
      graph_my->SetPointEYlow (iBin, yErrorLow );
      graph_my->SetPointEYhigh(iBin, yErrorHigh);

     
    }

  cleanGraph( graph_my );

  //    graph_my->Print();
  //  Info("getBandsRatio"," done");
  return graph_my;
}



void cleanGraph(TGraph* graph_my)
{
    if (graph_my == NULL ) return;
 
  for (Int_t iBin = 0; iBin <= graph_my->GetN(); iBin++)
    {
      Double_t xValue = 0, yValue = 0; 
      graph_my->GetPoint(iBin, xValue, yValue);
      if ( (0. == yValue) || TMath::IsNaN( yValue ) ) 
	{
	  graph_my->RemovePoint(iBin);
	  if ( iBin != graph_my->GetN() ) iBin--;
	}
    }
  
  return;
}
