#!/usr/bin/python
import os, sys
from ROOT import *
from array import array
param=sys.argv

File = "/home/jbossios/cern/HLLHC/Inputs/TriggerStudies/v1/JZAll.root"

Bins = ["00_05","05_10","10_15","15_20","20_25","25_30"]

####################################################################
gROOT.SetBatch(True)  # so does not pop up plots!

# ATLAS style
gROOT.LoadMacro("/home/jbossios/cern/AtlasStyle/AtlasStyle.C")
SetAtlasStyle()

# Load easyPlot
gROOT.LoadMacro("Plotter1D.cxx")
gROOT.LoadMacro("HelperClasses.cxx")
gROOT.LoadMacro("HelperFunctions.cxx")

# Plotter Instance for fomatting
Format = Plotter1D()   # This is the constructor for format only
Format.SetComparisonType("Normal")   # Will show second panel with ratio
Format.SetATLASlabel("Internal") # Other options: ATLAS, Internal, etc
#Format.SetRange("Y",10E-26,10E10)
#Format.SetAxisTitles("#it{p}_{T} [GeV]","d^{2}\sigma/d#it{p}_{T}dy [pb/GeV]") # x-axis, y-axis
Format.SetLegendPosition("Top")  # Other options: Top, TopLeft, Bottom
Format.SetLineWidthAll(2)
Format.SetDrawOptionAll("HIST][")
#Format.SetTLegendPosition(0.66,0.67,0.93,0.92)

# pT plot
OutPDF = "../Plots/TriggerStudies/pT"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
Plot.SetLogx()
Plot.SetLogy()
Plot.SetMoreLogLabelsX()
#Plot.SetRange("X",300,1000000)
Plot.AddHist("pT",File,"pT","P")
Plot.NoTLegend("pT")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("#it{p}_{T} [GeV]","Events") # x-axis, y-axis
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

# Timing plot
OutPDF = "../Plots/TriggerStudies/Timing"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
#Plot.SetRange("X",300,1000000)
Plot.AddHist("All",File,"Timing")
Plot.AddHist("HS jets",File,"Timing_HS")
Plot.AddHist("PU jets",File,"Timing_PU")
Plot.SetAxisTitles("Timing","Events") # x-axis, y-axis
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

# nConstituent plot
OutPDF = "../Plots/TriggerStudies/nConstituent"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
Plot.SetRange("X",0,45)
Plot.AddHist("nConstit",File,"nConstit")
Plot.NoTLegend("nConstit")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("Number of clusters","Events") # x-axis, y-axis
Plot.RebinAll(2)
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

# mu plot
OutPDF = "../Plots/TriggerStudies/mu"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
Plot.SetRange("X",180,220)
Plot.AddHist("mu",File,"mu")
Plot.NoTLegend("mu")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("actualInteractionsPerCrossing","Events") # x-axis, y-axis
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF

# timing vs mu plot
nMuBins = 8
muBins  = [180,185,190,195,200,205,210,215,220]
# get histogram
iFile   = TFile.Open(File)
Hist    = iFile.Get("h_timing_vs_mu")
Profile = Hist.ProfileY("timing_vs_mu_profile")
#for imu in range(0,nMuBins):
#  name  = "timing_vs_mu_"
#  name += str(imu)
#  low  = muHist.GetXaxis().FindBin(muBins[imu])
#  high = muHist.GetXaxis().FindBin(muBins[imu+1])-1
#  projection = muHist.ProjectionY(name,low,high,"e")
OutPDF = "../Plots/TriggerStudies/timing_vs_mu"
Plot = Plotter1D(OutPDF) # Name of the output file
Plot.ApplyFormat(Format)
#Plot.SetRange("X",180,220)
Plot.SetRange("Y",190,220)
Plot.AddHist("Profile",Profile)
Plot.NoTLegend("Profile")
Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
Plot.SetAxisTitles("actualInteractionsPerCrossing","<Timing>") # x-axis, y-axis
#Plot.SetLegendPosition("TopRight")  # Other options: Top, TopLeft, Bottom
#Plot.SetLegend(str(counter),IJXSLegends[counter])
Plot.Write()  # Saves plot into OutPDF



print ">>>> DONE <<<<"


