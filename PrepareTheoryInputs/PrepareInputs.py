#!/usr/bin/python
import os, sys
from ROOT import *
from array import array
param=sys.argv

CMEs = ["14","27"]
Bins = ["00_05","05_10","10_15","15_20","20_25","25_30"]

####################################################################
gROOT.SetBatch(True)  # so does not pop up plots!

for CME in CMEs:
  print("Running Dijets "+CME+" TeV")

  # PATH to input ROOT File
  PATH  = "/home/jbossios/cern/HLLHC/Inputs/Pavel/"
  PATH += CME
  PATH += "TeV/uncert/"

  # Loop over ystar bins
  counter = 0
  for bin in Bins:
    # Name of input file
    FileName = PATH+"dijet_HTystar_"+bin+"_R04_P2_ScaleMass_standTheoryUncert.root"
    # Open ROOT file
    File = TFile.Open(FileName)
    if not File:
      print(file+" not found, exiting")
      sys.exit()
    # Get Histogram
    HistTemp = File.Get("CT14nlo/scales/xsec")
    if not HistTemp:
      print("CT14nlo/scales/xsec not found, exiting")
      sys.exit()
    Hist = HistTemp.Clone(CME+str(counter))
    if not Hist:
      print("Histogram was not able to be cloned, exiting")
      sys.exit()
    if counter==1:
      for i in range(1,Hist.GetNbinsX()+1):
        if Hist.GetBinContent(i)!=0:
          Hist.SetBinContent(i,0)
          break
    elif counter==2:
      newcounter = 1
      for i in range(1,Hist.GetNbinsX()+1):
        if newcounter>2:
          break
        elif Hist.GetBinContent(i)!=0:
          Hist.SetBinContent(i,0)
          newcounter += 1
    elif counter==3:
      newcounter = 1
      for i in range(1,Hist.GetNbinsX()+1):
        if newcounter>4:
          break
        elif Hist.GetBinContent(i)!=0:
          Hist.SetBinContent(i,0)
          newcounter += 1
    elif counter==4:
      newcounter = 1
      for i in range(1,Hist.GetNbinsX()+1):
        if newcounter>5:
          break
        elif Hist.GetBinContent(i)!=0:
          Hist.SetBinContent(i,0)
          newcounter += 1
    elif counter==5:
      newcounter = 1
      for i in range(1,Hist.GetNbinsX()+1):
        if newcounter>3:
          break
        elif Hist.GetBinContent(i)!=0:
          Hist.SetBinContent(i,0)
          newcounter += 1
    NewFileName = PATH+"dijet_HTystar_"+bin+"_R04_P2_ScaleMass_standTheoryUncert_Final.root"
    NewFile = TFile(NewFileName,"RECREATE")
    Hist.Write()
    NewFile.Close()
    File.Close() 
    counter += 1

print("Running IJXS 14TeV")

# PATH to input ROOT File
PATH  = "/home/jbossios/cern/HLLHC/Inputs/Pavel/14TeV/uncert/"

# Loop over ystar bins
for bin in Bins:
  if bin!="25_30":
    continue
  # Name of input file
  FileName = PATH+"incljets_rapidity_"+bin+"_R04_P2_standTheoryUncert.root"
  # Open ROOT file
  File = TFile.Open(FileName)
  if not File:
    print(file+" not found, exiting")
    sys.exit()
  # Get Histogram
  HistTemp = File.Get("CT14nlo/scales/xsec")
  if not HistTemp:
    print("CT14nlo/scales/xsec not found, exiting")
    sys.exit()
  Hist = HistTemp.Clone("IJXS14TeV")
  if not Hist:
    print("Histogram was not able to be cloned, exiting")
    sys.exit()
  for i in range(1,Hist.GetNbinsX()+1):
    if Hist.GetBinCenter(i)>1000:
      Hist.SetBinContent(i,0)
  NewFileName = PATH+"incljets_rapidity_"+bin+"_R04_P2_standTheoryUncert_Final.root"
  NewFile = TFile(NewFileName,"RECREATE")
  Hist.Write()
  NewFile.Close()
  File.Close() 
  counter += 1


print ">>>> DONE <<<<"


