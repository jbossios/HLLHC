#!/usr/bin/python

import os, sys

param=sys.argv

# Date
LocalPath = "/afs/cern.ch/work/j/jbossios/public/HLLHC/xAODtoTTree"
date = "29_08_2018_HLLHC_Conservative_JERbugFix"
user = "jbossios"
MC15cPythia = False
HLLHCPythia = True
#HLLHCType = "Optimistic"
HLLHCType = "Conservative"

list=[
#    "JZ0", # NOT USED
#    "JZ1",
#    "JZ2",
#    "JZ3",
#    "JZ4",
    "JZ5",
#    "JZ6",
#    "JZ7",
]

##################################################################################
# DO NOT MODIFY
##################################################################################


if MC15cPythia:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ0":
      sample = "mc15_13TeV:mc15_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.merge.AOD.e3569_s2576_s2132_r7725_r7676"
    if JZ == "JZ1":
      sample = "mc15_13TeV:mc15_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.merge.AOD.e3569_s2576_s2132_r7725_r7676"
    if JZ == "JZ2":
      sample = "mc15_13TeV:mc15_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.merge.AOD.e3668_s2576_s2132_r7725_r7676"
    if JZ == "JZ3":
      sample = "mc15_13TeV:mc15_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.merge.AOD.e3668_s2576_s2132_r7725_r7676"
    if JZ == "JZ4":
      sample = "mc15_13TeV:mc15_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.merge.AOD.e3668_s2576_s2132_r7725_r7676"
    if JZ == "JZ5":
      sample = "mc15_13TeV:mc15_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.merge.AOD.e3668_s2576_s2132_r7725_r7676"
    if JZ == "JZ6":
      sample = "mc15_13TeV:mc15_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.merge.AOD.e3569_s2608_s2183_r7725_r7676"
    if JZ == "JZ7":
      sample = "mc15_13TeV:mc15_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.merge.AOD.e3668_s2608_s2183_r7725_r7676"

    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    command += " --submitDir="
    command += "Outputs_MC15cPythia_"
    command += JZ

    # Config
    command += " --config "+LocalPath+"/xAODAnaHelpers/data/xah_run_MC15c.json"

    # Prun options
    command += " --inputRucio prun --optGridMergeOutput=1 --optGridNFilesPerJob=1.0 --optRemoveSubmitDir 1"


    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > logfile_MC15cPythia_"
    command += JZ
    command += " 2> errors_MC15cPythia_"
    command += JZ

    command += " &"

    print command
    os.system(command)


if HLLHCPythia:
  for JZ in list:
    # Setting the correct sample
    if JZ == "JZ0":
      sample = "mc15_14TeV:mc15_14TeV.147910.Pythia8_AU2CT10_jetjet_JZ0W.recon.AOD.e2403_s3142_s3143_r9589"
    if JZ == "JZ1":
      sample = "mc15_14TeV:mc15_14TeV.147911.Pythia8_AU2CT10_jetjet_JZ1W.recon.AOD.e2403_s3142_s3143_r9589"
    if JZ == "JZ2":
      sample = "mc15_14TeV:mc15_14TeV.147912.Pythia8_AU2CT10_jetjet_JZ2W.recon.AOD.e2403_s3142_s3143_r9589"
    if JZ == "JZ3":
      sample = "mc15_14TeV:mc15_14TeV.147913.Pythia8_AU2CT10_jetjet_JZ3W.recon.AOD.e2403_s3142_s3143_r9589"
    if JZ == "JZ4":
      sample = "mc15_14TeV:mc15_14TeV.147914.Pythia8_AU2CT10_jetjet_JZ4W.recon.AOD.e2403_s3142_s3143_r9589"
    if JZ == "JZ5":
      sample = "mc15_14TeV:mc15_14TeV.147915.Pythia8_AU2CT10_jetjet_JZ5W.recon.AOD.e2403_s3142_s3143_r9589"
    if JZ == "JZ6":
      sample = "mc15_14TeV:mc15_14TeV.147916.Pythia8_AU2CT10_jetjet_JZ6W.recon.AOD.e2403_s3142_s3143_r9589"
    if JZ == "JZ7":
      sample = "mc15_14TeV:mc15_14TeV.147917.Pythia8_AU2CT10_jetjet_JZ7W.recon.AOD.e2403_s3142_s3143_r9589"
 
    command = "xAH_run.py "

    # Input dataset
    command += "--files "
    command += sample

    command += " --submitDir="
    command += "Outputs_HLLHCPythia_"
    command += JZ

    # Config
    command += " --config "+LocalPath+"/xAODAnaHelpers/data/xah_run_HLLHC"
    if HLLHCType is "Conservative":
      command += "_conservative.json"
    elif HLLHCType is "Optimistic":
      command += "_optimistic.json"
    else:
      print "HLLHCType not known, exiting"
      sys.exit(0)

    # Prun options
    command += " --inputRucio prun --optGridNFilesPerJob=40.0 --optRemoveSubmitDir 1"

    # Output dataset
    command += " --optGridOutputSampleName=user."+user+"."
    command += date
    command += "."
    command += JZ

    # Logfiles
    command += " > logfile_HLLHCPythia_"
    command += HLLHCType
    command += "_"
    command += JZ
    command += " 2> errors_HLLHCPythia_"
    command += HLLHCType
    command += "_"
    command += JZ

    command += " &"

    print command
    os.system(command)



