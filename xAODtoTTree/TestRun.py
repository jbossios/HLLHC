#!/usr/bin/python

import os, sys

param=sys.argv

##################################################################################
# Edit
##################################################################################

MC15c = False
HLLHC = True
HLLHCType = "_conservative"

# pcuba002
MC15cSample = "/1/jbossios/SM_testFiles/MC15c/mc15_13TeV/DAOD_JETM9.09118579._000043.pool.root.1"
HLLHCSample = "/eos/atlas/user/j/jbossios/HLLHC/mc15_14TeV/AOD.11442181._020691.pool.root.1"

##################################################################################
# DO NOT MODIFY
##################################################################################

if MC15c and HLLHC:
  print "Choose one, MC15c or HLLHC sample, exiting"
  sys.exit()

command = ""

if HLLHC:
  command  = "xAH_run.py --files "+HLLHCSample+" --config /afs/cern.ch/user/j/jbossios/work/public/HLLHC/xAODtoTTree/xAODAnaHelpers/data/xah_run_HLLHC"
  command += HLLHCType
  command += ".json --submitDir outputTreesHLLHC direct > Logs/log_HLLHC 2> Logs/err_HLLHC &"

if MC15c:
  command = "xAH_run.py --files "+MC15cSample+" --config /afs/cern.ch/user/j/jbossios/work/public/HLLHC/xAODtoTTree/xAODAnaHelpers/data/xah_run_MC15c.json --submitDir outputTreesMC15c direct > Logs/log_MC15c 2> Logs/err_MC15c &"

print command
os.system(command)

