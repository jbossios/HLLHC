#!/usr/bin/python
import os, sys
from ROOT import *
param=sys.argv

MC15c = False
HLLHC_optimistic   = False
HLLHC_conservative = True
Debug = False # More verbosity and run over only 10 events

systs_MC15c = [
  "JET_EffectiveNP_1__1up", # DONE
  "JET_EffectiveNP_1__1down", # DONE
  "JET_EffectiveNP_2__1up", # DONE
  "JET_EffectiveNP_2__1down", # DONE
  "JET_EffectiveNP_3__1up", # DONE
  "JET_EffectiveNP_3__1down", # DONE
  "JET_EffectiveNP_4__1up", # DONE
  "JET_EffectiveNP_4__1down", # DONE
  "JET_EffectiveNP_5__1up", # DONE
  "JET_EffectiveNP_5__1down", # DONE
  "JET_EffectiveNP_6__1up", # DONE
  "JET_EffectiveNP_6__1down", # DONE
  "JET_EffectiveNP_7__1up", # DONE
  "JET_EffectiveNP_7__1down", # DONE
  "JET_EffectiveNP_8restTerm__1up", # DONE
  "JET_EffectiveNP_8restTerm__1down", # DONE
  "JET_EtaIntercalibration_Modelling__1up", # DONE
  "JET_EtaIntercalibration_Modelling__1down", # DONE
  "JET_EtaIntercalibration_NonClosure__1up", # DONE
  "JET_EtaIntercalibration_NonClosure__1down", # DONE
  "JET_EtaIntercalibration_TotalStat__1up", # DONE
  "JET_EtaIntercalibration_TotalStat__1down", # DONE
  "JET_Flavor_Composition__1up", # DONE
  "JET_Flavor_Composition__1down", # DONE
  "JET_Flavor_Response__1up", # DONE
  "JET_Flavor_Response__1down", # DONE
  "JET_Pileup_OffsetMu__1up", # DONE
  "JET_Pileup_OffsetMu__1down", # DONE
  "JET_Pileup_OffsetNPV__1up", # DONE
  "JET_Pileup_OffsetNPV__1down", # DONE
  "JET_Pileup_PtTerm__1up", # DONE
  "JET_Pileup_PtTerm__1down", # DONE
  "JET_Pileup_RhoTopology__1up", # DONE
  "JET_Pileup_RhoTopology__1down", # DONE
  "JET_PunchThrough_MC15__1up", # DONE
  "JET_PunchThrough_MC15__1down", # DONE
  "JET_SingleParticle_HighPt__1up", # DONE
  "JET_SingleParticle_HighPt__1down", # DONE
  "JET_JER_SINGLE_NP__1up", # DONE
]

systs_HLLHC_optimistic=[ # HLLHC optimistic
  "JET_EffectiveNP_1__1up", # DONE
  "JET_EffectiveNP_1__1down", # DONE
  "JET_EffectiveNP_2__1up",
  "JET_EffectiveNP_2__1down",
  "JET_EffectiveNP_3__1up",
  "JET_EffectiveNP_3__1down",
  "JET_EffectiveNP_4__1up",
  "JET_EffectiveNP_4__1down",
  "JET_EffectiveNP_5__1up",
  "JET_EffectiveNP_5__1down",
  "JET_EffectiveNP_6__1up",
  "JET_EffectiveNP_6__1down",
  "JET_EffectiveNP_7__1up",
  "JET_EffectiveNP_7__1down",
  "JET_EffectiveNP_8restTerm__1up",
  "JET_EffectiveNP_8restTerm__1down",
  "JET_Flavor_Response__1up",
  "JET_Flavor_Response__1down",
  "JET_Pileup_RhoTopology__1up",
  "JET_Pileup_RhoTopology__1down",
  "JET_JER_SINGLE_NP__1up",
]

systs_HLLHC_conservative=[ # HLLHC conservative
  "JET_EffectiveNP_1__1up",
  "JET_EffectiveNP_1__1down",
  "JET_EffectiveNP_2__1up",
  "JET_EffectiveNP_2__1down",
  "JET_EffectiveNP_3__1up",
  "JET_EffectiveNP_3__1down",
  "JET_EffectiveNP_4__1up",
  "JET_EffectiveNP_4__1down",
  "JET_EffectiveNP_5__1up",
  "JET_EffectiveNP_5__1down",
  "JET_EffectiveNP_6__1up",
  "JET_EffectiveNP_6__1down",
  "JET_EffectiveNP_7__1up",
  "JET_EffectiveNP_7__1down",
  "JET_EffectiveNP_8restTerm__1up",
  "JET_EffectiveNP_8restTerm__1down",
  "JET_EtaIntercalibration_Modelling__1up",
  "JET_EtaIntercalibration_Modelling__1down",
  "JET_Flavor_Response__1up",
  "JET_Flavor_Response__1down",
  "JET_Pileup_RhoTopology__1up",
  "JET_Pileup_RhoTopology__1down",
  "JET_JER_SINGLE_NP__1up",
]


####################################################################################
# DO NOT MODIFY
####################################################################################

# Protection
if HLLHC_optimistic and HLLHC_conservative:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if HLLHC_conservative and MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if HLLHC_optimistic and MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)
if not HLLHC_optimistic and not HLLHC_conservative and not MC15c:
  print "Choose one, HLLHC_optimistic, HLLHC_conservative or MC15c, exiting"
  sys.exit(0)

Systs = []
if HLLHC_conservative:
  Systs = systs_HLLHC_conservative
elif HLLHC_optimistic:
  Systs = systs_HLLHC_optimistic
elif MC15c:
  Systs = systs_MC15c

HistName =  "j_pT"

# Get Nominal Histogram
NomNameFile = "../Analysis/TreetoHists/Results/"
if MC15c:
  NomNameFile += "MC15c/"
if HLLHC_conservative:
  NomNameFile += "HLLHC_conservative/"
if HLLHC_optimistic:
  NomNameFile += "HLLHC_optimistic/"
NomNameFile += "nominal.root"
NomFile = TFile.Open(NomNameFile)
if not NomFile:
  print "NomFile not found, exiting"
  sys.exit(0)
NomHist = NomFile.Get(HistName)
if not NomHist:
  print "NomHist not found, exiting"
  sys.exit(0)

# Output File
OutNameFile = ""
if MC15c:
  OutNameFile += "MC15c"
if HLLHC_conservative:
  OutNameFile += "HLLHC_conservative"
if HLLHC_optimistic:
  OutNameFile += "HLLHC_optimistic"
OutNameFile += ".root"
OutFile = TFile(OutNameFile,"RECREATE")

for syst in Systs:
  # Get Systematic variation Histogram
  print "Syst: "+syst
  VarNameFile = "../Analysis/TreetoHists/Results/"
  if MC15c:
    VarNameFile += "MC15c/"
  if HLLHC_conservative:
    VarNameFile += "HLLHC_conservative/"
  if HLLHC_optimistic:
    VarNameFile += "HLLHC_optimistic/"
  VarNameFile += syst
  VarNameFile += ".root"
  VarFile = TFile.Open(VarNameFile)
  if not VarFile:
    print "VarFile not found, exiting"
    sys.exit(0)
  VarFile.cd()
  VarHist = VarFile.Get(HistName)
  if not VarHist:
    print "VarHist not found, exiting"
    sys.exit(0)
  # Calculate Systematic Uncertainty
  UncerHist = VarHist.Clone(syst)
  UncerHist.Add(NomHist,-1.)
  UncerHist.Divide(NomHist)
  OutFile.cd()
  UncerHist.Write()
  VarFile.Close()

NomFile.Close()
OutFile.Close()
print "OutputFile: "+OutNameFile
print ">>>> DONE <<<<"


